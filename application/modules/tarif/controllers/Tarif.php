<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarif extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'report');

	public function __construct()
	{
		parent::__construct();
		check_login();
	}

	public function index()
	{

		$user_data['data_ref'] = $this->data_ref;

     	$this->load->view('template/header');
		$this->load->view('view',$user_data);
		$this->load->view('template/footer');
	}

	public function proses(){
		$this->load->view('template/header_kosong');
		$this->load->view('laporan');
	}


	public function excel(){
		$this->load->view('excel');
	}

		

	

}
