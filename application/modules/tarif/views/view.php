  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">TARIF DASAR PELAYANAN JASA PEMANDUAN DAN PENUNDAAN KAPAL </h3>

              <div class="card-tools">
               
              </div>
              <!-- /.card-tools -->
        </div>


        <?php
        $conf = $this->db->query("SELECT * FROM kurs_mata_uang")->row_array();
        ?>
        <!-- /.card-header -->
        <div class="card-body">

           <form action="<?=base_url('report/proses');?>" target="_blank" class="form-horizontal" method="POST" >
                    <div class="form-body">

                    <h4>Tarif Jasa Pemanduan</h4>
                    <table class="table table-bordered">
                        <tr>
                            <td width="5%" rowspan="2" valign="middle">No</td>
                            <td width="25%" rowspan="2" align="center">Uraian</td>
                            <td colspan="2" width="30%" align="center">Besaran Tarif</td>
                            <td width="40%" rowspan="2" align="center">Satuan</td>
                        </tr>
                        <tr>
                            <td width="15%" align="center">Dalam Negeri</td>
                            <td width="15%" align="center">Luar Negeri</td>
                        </tr>

                        <?php
                        $no = 1;
                        $tarif = $this->db->query("SELECT * FROM tarif WHERE jenis='PANDU'")->result();
                        foreach ($tarif as $trf){
                            echo 
                            '<tr>
                                <td>'.$no++.'</td>
                                <td colspan="4"><b>'.$trf->uraian.'</b></td>
                            </tr>
                            <tr>
                                <td>'.$no++.'</td>
                                <td>Tarif Tetap</td>
                                <td><input class="form-control" name="a" value="'.rupiah($trf->tarif_tetap_lokal,3).'" style="text-align:right;"></td>
                                <td><input class="form-control" name="a"  value="'.rupiah($trf->tarif_tetap_asing,3).'" style="text-align:right;"></td>
                                <td>Per Kapal Per Gerakan</td>
                            </tr>
                            <tr>
                                <td>'.$no++.'</td>
                                <td>Tarif Variabel</td>
                                <td><input class="form-control" name="a" value="'.rupiah($trf->tarif_variabel_lokal,3).'" style="text-align:right;"></td>
                                <td><input class="form-control" name="a"  value="'.rupiah($trf->tarif_variabel_asing,3).'" style="text-align:right;"></td>
                                <td>Per GT Per Kapal Per Gerakan</td>
                            </tr>';
                        }
                        ?>
                    </table>


                        <br>
                        <h4>Tarif Jasa Penundaan Kapal</h4>
                    <table class="table table-bordered">
                        <tr>
                            <td width="5%" rowspan="2" valign="middle">No</td>
                            <td width="25%" rowspan="2" align="center">Uraian</td>
                            <td colspan="2" width="30%" align="center">Besaran Tarif</td>
                            <td width="40%" rowspan="2" align="center">Satuan</td>
                        </tr>
                        <tr>
                            <td width="15%" align="center">Dalam Negeri</td>
                            <td width="15%" align="center">Luar Negeri</td>
                        </tr>

                        <?php
                        $no = 1;
                        $tarif = $this->db->query("SELECT * FROM tarif WHERE jenis='TUNDA'")->result();
                        foreach ($tarif as $trf){
                            echo 
                            '<tr>
                                <td>'.$no++.'</td>
                                <td colspan="4"><b> Kapal '.rupiah($trf->gt_bawah).' s.d '.rupiah($trf->gt_atas).' GT</b></td>
                            </tr>
                            <tr>
                                <td>-</td>
                                <td>Tarif Tetap</td>
                                <td><input class="form-control" name="a" value="'.rupiah($trf->tarif_tetap_lokal,3).'" style="text-align:right;"></td>
                                <td><input class="form-control" name="a"  value="'.rupiah($trf->tarif_tetap_asing,3).'" style="text-align:right;"></td>
                                <td>Per Unit Kapal Tunda / Jam</td>
                            </tr>
                            <tr>
                                <td>-</td>
                                <td>Tarif Variabel</td>
                                <td><input class="form-control" name="a" value="'.rupiah($trf->tarif_variabel_lokal,3).'" style="text-align:right;"></td>
                                <td><input class="form-control" name="a"  value="'.rupiah($trf->tarif_variabel_asing,3).'" style="text-align:right;"></td>
                                <td>Per GT Kapal yang ditunda/ Unit Kapal Tunda/ Jam</td>
                            </tr>';
                        }
                        ?>
                    </table>


                        <div class="form-group row">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-sm" type="submit">Simpan Perubahan</button>
                            </div>
                        </div>


                    </div>
                </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>
