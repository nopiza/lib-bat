<?php
$awal = $this->input->post('periode_awal');
$akhir = $this->input->post('periode_akhir');

?>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Report Permohonan Pandu Tunda <br>
            Periode : <?=tgl_indo($awal);?> - <?=tgl_indo($akhir);?></h3>

              <div class="card-tools">
                <a href="<?=base_url('report/excel/'.$awal.'/'.$akhir);?>" class="btn btn-secondary btn-sm" ><i class="fa fa-plus"></i> Import to Excel</a>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

           <table class="table table-bordered">
             <tr>
               <th width="5%">No.</th>
               <th>Tanggal</th>
               <th>No. Registrasi</th>
               <th>Nama Kapal</th>
               <th>Keagenan</th>
               <th>Jenis Kapal</th>
               <th>GRT</th>
               <th>User Operator</th>
             </tr>

<?php
$no=1;


$laporan = $this->db->query("SELECT * FROM permohonan 
  WHERE tanggal >= '$awal' AND tanggal <= '$akhir' ")->result();
foreach ($laporan as $lpr) {

  echo '
  <tr>
               <td>'.$no++.'</td>
               <td>'.tgl_indo($lpr->tanggal).'</td>
               <td>'.$lpr->no_permohonan.'</td>
               <td>'.$lpr->nama_kapal.'</td>
               <td>'.$lpr->keagenan.'</td>
               <td>'.$lpr->jenis_pelayaran_kapal.'</td>
               <td>'.$lpr->berat_kotor.'</td>
               <td>Heru Hidayat</td>

             </tr>';
}
?>
             
           </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
</div>


</body>
</html>

