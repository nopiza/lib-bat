<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppk extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'ppk');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ppk_model','ppk');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('view',$user_data);
	}

	public function detail($id)
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$user_data['pkk'] = $this->db->query("SELECT * FROM pkk WHERE id='$id'")->row_array();
      	$this->load->view('template/header',$user_data);
      	$this->load->view('detail',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->ppk->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-info" href="'.base_url('pkk/detail/'.$post->id).'"> View Detail</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nomor_pkk;
			$row[] = $post->nama_kapal;
			$row[] = $post->jenis_kapal;
			$row[] = $post->nama_perusahaan;
			$row[] = '';
			//add html for action
			$row[] = $link_edit;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->ppk->count_all(),
						"recordsFiltered" => $this->ppk->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->ppk->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$data = array(
			'tipe_kendaraan' 		=> $this->input->post('tipe_kendaraan'),
			'plat_nomor' 			=> $this->input->post('plat_nomor'),
			'keterangan' 			=> $this->input->post('keterangan')
		);
		$this->ppk->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'tipe_kendaraan' 		=> $this->input->post('tipe_kendaraan'),
			'plat_nomor' 			=> $this->input->post('plat_nomor'),
			'keterangan' 			=> $this->input->post('keterangan')
		);

		$this->ppk->update(array('id_pkk' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('ppk',array('id_pkk'=>$id));
		echo json_encode(array("status" => TRUE));
	}

}
