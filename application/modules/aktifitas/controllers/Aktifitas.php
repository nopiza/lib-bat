<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktifitas extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'aktifitas');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Aktifitas_model','aktifitas');
		// $this->load->model('Group/Group_model','group');
		check_login();
	}

	public function index()
	{

		$user_data['data_ref'] = $this->data_ref;

     	if($this->encryption->decrypt($this->session->userdata('is_admin')) == '3'){

	     	$this->load->view('template/header');
			$this->load->view('view',$user_data);
		}else{
			$user_data['alert'] = '0';
			$id = $this->encryption->decrypt($this->session->userdata('id'));
			$user_data['pengguna'] = $this->db->query("SELECT * FROM users WHERE id='$id'")->row_array();
			$this->load->view('template/header');
			$this->load->view('view_user',$user_data);
		}

	}

	public function ajax_list()
	{
		$list = $this->aktifitas->get_datatables();
		$data = array();
		$no = $_POST['start'];


		foreach ($list as $post) {


				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';

				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';


			$no++;
			$row = array();
         	$row[] = $no;
			 $row[] = tgl_indo($post->tanggal);
         	$row[] = $post->surname;
			$row[] = $post->username;
         	$row[] = $post->aktifitas;
         	$row[] = $post->keterangan;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->aktifitas->count_all(),
						"recordsFiltered" => $this->aktifitas->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	

}
