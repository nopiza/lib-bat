  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <br>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Pembuatan SPB</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                       <div class="form-group row">
                            <label class="control-label col-md-2">No. SPB</label>
                            <div class="col-md-3">
                                <input name="no_spb" class="form-control" type="text" id="no_spb" readonly="" value="<?=$trans['no_transaksi'];?>">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Ship To</label>
                            <div class="col-md-6">
                                <input name="no_spb" class="form-control" type="text" id="no_spb" readonly="" value="<?=$trans['nama_vendor'];?>">
                                <span class="help-block"></span>
                            </div>
                        </div>


                    </div>
                </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Detail Barang</h3>

              <div class="card-tools">
                <a class="btn btn-info btn-sm" onclick="add('1')"><i class="fa fa-plus"></i> Item Repair</a>
                <a class="btn btn-warning btn-sm" onclick="add('2')"><i class="fa fa-plus"></i> Item Other</a>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

          <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="35%">Description</th>
                    <th width="10%">Coli /Kg</th>
                    <th width="10%">Item</th>
                    <th width="15%">Reference</th>
                    <th width="15%">Remark</th>
                    <th width="10%">Del</th>
                </tr>
            </thead>
            <tbody id="isitem">
              <!-- <div id="isitem"></div> -->
              <!-- <tr>
            <td> 1</td>
            <td> 2</td>
            <td> 3</td>
            <td> 4</td>
            <td> 5</td>
            <td> 5</td>
            <td> 5</td>
        </tr> -->
            </tbody>
          </table>
        </div>
      </div>

      <div id="batas"></div>

    </section>
</div>


</body>
</html>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">header</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" name="id_transaksi" value="<?=$trans['id_transaksi'];?>" /> 
                    <div class="form-body">

    
                        <!-- <div class="form-group row">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-4">
                                <input name="part_name" placeholder="" class="form-control" type="text" id="part_name">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4">
                                <input name="part_number" placeholder="" class="form-control" type="text" id="pn">
                                <span class="help-block"></span>
                            </div>
                        </div> -->


                        <div class="form-group row">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-8">
                                <textarea name="description" placeholder="" class="form-control" rows="4"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Coli / Kg</label>
                            <div class="col-md-4">
                                <input name="coli" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Item</label>
                            <div class="col-md-4">
                                <input name="items" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Reference</label>
                            <div class="col-md-4">
                                <input name="reference" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Remark</label>
                            <div class="col-md-7">
                                <!-- <input name="remark" placeholder="" class="form-control" type="text"> -->
                                <textarea name="remark" placeholder="" class="form-control" rows="4"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_repair" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">header</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_repair" class="form-horizontal">
                    <input type="hidden" name="id_transaksi" value="<?=$trans['id_transaksi'];?>" /> 
                    <div class="form-body">

    
                        <div class="form-group row">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-4">
                                <input name="part_number" placeholder="" class="form-control" type="text" id="part_number">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4">
                                <input name="part_name" placeholder="" class="form-control" type="text" id="pn">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">SN & CN</label>
                            <div class="col-md-8">
                                <input name="pncn" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label col-md-3">Coli / Kg</label>
                            <div class="col-md-4">
                                <input name="coli" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Item</label>
                            <div class="col-md-4">
                                <input name="items" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Reference</label>
                            <div class="col-md-4">
                                <input name="reference" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-3">Remark</label>
                            <div class="col-md-7">
                                <input name="remark" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_repair()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?php  $this->load->view('template/footer'); ?>


<script src="<?php echo base_url('assets/admin/plugins/select2/select2.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/plugins/select2/select2-bootstrap.css') ?>">

<script type="text/javascript">

var url_apps = "<?=base_url();?>"

$(document).ready(function () {
//----->
//Ambil semua data customer untuk select 2
  $("#nama_vendor").select2({
    ajax: {
      url: url_apps+'transaksi/ajax_select_customer',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params, // search term
        };
      },
      results: function (data, params) {
        console.log(data);
        return {
            results: $.map(data, function (item) {
                return {
                    text: item.nama_vendor,
                    id: item.id_vendor
                }
            })
        };
      },
      cache: true
    },
    minimumInputLength: 1,
  });  


  $("#part_number").select2({
    ajax: {
      url: url_apps+'transaksi/ajax_select_part',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params, // search term
        };
      },
      results: function (data, params) {
        console.log(data);
        return {
            results: $.map(data, function (item) {
                return {
                    text: item.part_number,
                    id: item.part_number
                }
            })
        };
      },
      cache: true
    },
    minimumInputLength: 1,
  });  


});


$('#nama_vendor').on('change', function() {
  var idSiswa = $(this).val();
  $.ajax({
    url: url_apps + 'transaksi/get/' + $(this).val(),
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    //alert(data.ALAMAT);
    $('#singkatan').val(data.singkatan);
    
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
});


$('#part_number').on('change', function() {
  var idSiswa = $(this).val();
  $.ajax({
    url: url_apps + 'transaksi/get_part/' + $(this).val(),
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    //alert(data.ALAMAT);
    $('#pn').val(data.part_name);
    
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
});


   function add(id)
   {
       save_method = 'add';
       $('#form')[0].reset(); // reset form on modals
       $('#form_repair')[0].reset(); // reset form on modals
       $('.form-group').removeClass('has-error'); // clear error class
       $('.help-block').empty(); // clear error string

       if(id == '1'){
          $('#modal_form_repair').modal('show'); // show bootstrap modal
       }else{
          $('#modal_form').modal('show'); // show bootstrap modal
       }
       
       $('.modal-title').text('Tambah Item'); // Set Title to Bootstrap modal title
   }


   function save()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url;
    
       if(save_method == 'add') {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add')?>";
       } else {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update')?>";
       }
    
       // ajax adding data to database
       var formData = new FormData($('#form')[0]);
       $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                   $('#modal_form').modal('hide');
                   // reload_table();
                   $("#isitem").load("<?php $id = $this->uri->segment(3); echo base_url('transaksi/isitem/'.$id);?>");
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }




   function save_repair()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url;
    
       if(save_method == 'add') {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_add_repair')?>";
       } else {
           url = "<?php echo site_url($data_ref['uri_controllers'].'/ajax_update')?>";
       }
    
       // ajax adding data to database
       var formData = new FormData($('#form_repair')[0]);
       $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
           success: function(data)
           {
    
               if(data.status) //if success close modal and reload ajax table
               {
                   $('#modal_form_repair').modal('hide');
                   // reload_table();
                   $("#isitem").load("<?php $id = $this->uri->segment(3); echo base_url('transaksi/isitem/'.$id);?>");
                   Lobibox.notify('success', {
                       size: 'mini',
                       msg: 'Data berhasil Disimpan'
                   });
               }
               else
               {
                   for (var i = 0; i < data.inputerror.length; i++) 
                   {
                       $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                       $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                   }
               }
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }


   $(document).ready(function() {
        $("#isitem").load("<?php $id = $this->uri->segment(3); echo base_url('transaksi/isitem/'.$id);?>");
    });

</script>


