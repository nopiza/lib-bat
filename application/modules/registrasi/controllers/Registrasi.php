<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'registrasi');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Registrasi_model','registrasi');
		// $this->load->model('Group/Group_model','group');
		check_login();
	}

	public function index()
	{

		$user_data['data_ref'] = $this->data_ref;
		$user_data['title'] = 'Menu';
		$this->load->view('template/header');
		$this->load->view('view',$user_data);
	}

	public function ajax_list()
	{

		$list = $this->registrasi->get_datatables();
		$data = array();
		$no = $_POST['start'];


		foreach ($list as $post) {

			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Verifkasi</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_agen;
         	$row[] = $post->alamat;
         	$row[] = $post->username;
         	$row[] = $post->no_wa;
			if($post->stt_registrasi == '0'){
				$row[] = '<span class="badge badge-pill badge-danger">Registrasi</span>';
			}else if($post->stt_registrasi == '1'){
				$row[] = '<span class="badge badge-pill badge-success">Verifikasi</span>';
			}
         	
			//add html for action
			$row[] = $link_edit.$link_hapus;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->registrasi->count_all(),
						"recordsFiltered" => $this->registrasi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->registrasi->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		aktifitas('Login','Melakukan Penambahan registrasi');
		$data = array(
				'nama_agen' 		=> $this->input->post('nama_agen'),
				'nama_singkat' 		=> $this->input->post('nama_singkat'),
				'nama_kepala' 		=> $this->input->post('nama_kepala'),
				'npwp' 			=> $this->input->post('npwp'),
            	'email' 		=> $this->input->post('email'),
            	'no_wa' 		=> $this->input->post('no_wa'),
            	'alamat' 		=> $this->input->post('alamat'),
				'username' 		=> $this->input->post('username'),
            	'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT)
		);

		$insert = $this->registrasi->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		aktifitas('Login','Melakukan Edit Data registrasi');

			$data = array(
				'stt_registrasi' 			=> '1'
			);
		
		$this->registrasi->update(array('id' => $this->input->post('id')), $data);

		// isikan ke tabel agen
		$data = array(
			'nama_agen' 		=> $this->input->post('nama_agen'),
			'email' 			=> $this->input->post('email'),
			'kontak' 			=> $this->input->post('no_wa'),
			'alamat' 			=> $this->input->post('alamat'),
			'username' 			=> $this->input->post('username'),
			'password' 			=> password_hash($this->input->post('password'), PASSWORD_DEFAULT)
		);

		$this->db->insert('data_keagenan', $data);
		//Kirim data registrasi ke internal ------------------------------------------------------------>
//cari nomor penerima
$wa = trim($this->input->post('no_wa'));
          $wa = str_replace(' ','',$wa);
          $wa = str_replace('-','',$wa);
          $belakang = substr($wa,1);
          $awal = substr($wa,0,1);
          
          if($awal == '0'){
            $nowa = '62'.$belakang;
          }else{
            $nowa = $wa;
          }
          
$penerima = $nowa;

$pesan = '*siPanda-LIB*
-------------------------------

Data registrasi anda atas nama : 
*'.$this->input->post('nama_agen').'* 
telah kami verifikasi.

Anda dapat login melalui link berikut : 
https://agen.lintasinternasionalberkarya.com
*Username :* '.$this->input->post('username').'
*Password :* '.$this->input->post('password').'

Jika terdapat kendala dalah penggunaan aplikasi silahkan menghubungi kami.

*Terima Kasih.*';

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://app.whatspie.com/api/messages',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => 'receiver='.$penerima.'&device=6282198700818&message='.$pesan.'&type=chat',
				CURLOPT_HTTPHEADER => array(
				'Accept: application/json',
				'Content-Type: application/x-www-form-urlencoded',
				'Authorization: Bearer GwD2FV73AgEoHmlB4lvqPp1OfC5ydnR3o2c1REEd33HfASB3zV'
				),
			));
			$response = curl_exec($curl);
			curl_close($curl);			

		echo json_encode(array("status" => TRUE));
	}


	public function update_user()
	{
		aktifitas('Login','Melakukan Update Data Agen');
		if($this->input->post('password') == ''){
			$data = array(
				'surname' 		=> $this->input->post('surname'),
				'username' 		=> $this->input->post('username'),

			);
		}else{
			$data = array(
				'surname' 		=> $this->input->post('surname'),
				'username' 		=> $this->input->post('username'),
            	'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			);
		}
		
		
		$this->registrasi->update(array('id' => $this->input->post('id')), $data);
		// echo json_encode(array("status" => FALSE));
		// redirect('pengguna');
		$user_data['alert'] = '1';
		$id = $this->encryption->decrypt($this->session->userdata('id'));
		$user_data['registrasi'] = $this->db->query("SELECT * FROM users WHERE id='$id'")->row_array();
		$this->load->view('template/header');
		$this->load->view('view_user',$user_data);
	}

	public function ajax_delete($id)
	{
		aktifitas('Login','Melakukan Hapus Data registrasi');
		$this->registrasi->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

}
