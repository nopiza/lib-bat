  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Soap Client</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Proses Client</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">List Soap Client</h3>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="80%">Endpoint Inaportnet</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>authenticate<br><span class="badge badge-pill badge-success">Cek User dan Password</span></td>
                        <td><a href="<?=base_url('apiclient/authenticate');?>" class="btn btn-sm btn-secondary">Cek Data Data</a></td>
                    </tr>

                    <tr>
                        <td>2</td>
                        <td>entryRpkro<br><span class="badge badge-pill badge-success">Rencana Pelayanan Kapal dan Rencana Operasi</span></td>
                        <td><a href="<?=base_url('apiclient/entryRpkro');?>" class="btn btn-sm btn-primary">Kirim Data</a></td>
                    </tr>

                    <tr>
                        <td>3</td>
                        <td>SetSpkPandu</td>
                        <td><a href="<?=base_url('apiclient/SetSpkPandu');?>" class="btn btn-sm btn-primary">Kirim Data</a></td>
                    </tr>

                    <tr>
                        <td>4</td>
                        <td>getEntryPKK<br><span class="badge badge-pill badge-success">Mengambil detail data PKK, sesuai No. PKK</span></td>
                        <td><a href="<?=base_url('apiclient/getEntryPKK');?>" class="btn btn-sm btn-success">Ambil Data</a></td>
                    </tr>

                    <tr>
                        <td>5</td>
                        <td>getEntryRKBM</td>
                        <td><a href="<?=base_url('apiclient/getEntryRKBM');?>" class="btn btn-sm btn-success">Ambil Data</a></td>
                    </tr>

                    <tr>
                        <td>6</td>
                        <td>getEntrySPB</td>
                        <td><a href="<?=base_url('apiclient/getEntrySPB');?>" class="btn btn-sm btn-success">Ambil Data</a></td>
                    </tr>

                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>