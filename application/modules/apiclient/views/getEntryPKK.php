  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Soap Client - getEntryPKK</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Kirim Data</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Kirim Data</h3>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

        <form action="#" id="form" class="form-horizontal">

            <div class="form-group row">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-md-5">
                <input name="username" type="text" class="form-control" id="username" autocomplete="OFF" value="lintas-bontang">
                <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-md-5">
                <input name="password" type="password" class="form-control" id="password" value="23Okto1991">
                <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 control-label">Nomor PKK</label>
                <div class="col-md-5">
                <input name="nomorPKK" type="text" class="form-control" id="nomorPKK">
                <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 control-label"></label>
                <div class="col-md-5">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary btn-sm">Kirim Data</button>
                </div>
            </div>

            
        </form>

        <br>
        <hr>

        <pre id="beautified"></pre>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>

<script>

function save()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url = "<?php echo site_url('apiclient/getEntryPKK_response')?>";

       // ajax adding data to database
       $.ajax({
           url : url,
           type: "POST",
           data: $('#form').serialize(),
           dataType: "JSON",
           success: function(data)
           {
    

                // Lobibox.notify('success', {
                //     size: 'mini',
                //     msg: 'Data berhasil Disimpan'
                // });
                document.getElementById("beautified").innerHTML = JSON.stringify(data, undefined, 2);
                $('#btnSave').text('Simpan'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }


    // var data = {"authenticateResult":{"statusCode":"01","statusMessage":"Sukses","userInfo":{"id":47741,"username":"Lintas-bontang","email":"lintasinternasionalberkarya@gmail.com","status":"10","kode_pelabuhan":"IDBXT","fullname":"Edo nusa herlangga","auth_name":"BUP","jabatan":"staff","bidang":"","login_attempt":"0"},"organisasi":{"kode_kantor":"KNT.2208.000001","kode_perusahaan":"PRS.2208.000001","nama":"PT.lintas internasional berkarya","npwp":"\/v1\/doman\/download\/get?namafile=42.347.324.8-721.000","alamat":"JL.AHMAD YANI,PERUM HALAL SQUARE BLOCK G NO 7 ,BONTANG UTARA KOTA BONTANG","siupal":"1223000502605","tgl_siupal":"2021-05-25","akta":"04","golongan_usaha":"BUP","kode_tipe_perusahaan":"BUP","firma":"PT. ","domisili":"64.74","telp":"08115954455","fax":"","penanggung_jawab":"MARGO SUCIPTO","ktp":"20220801032813.KTP_penaggung_jawab.pdf","kode_wilayah":"64.74","tgl_registrasi":"2022-08-01 03:28:13","tgl_expired":"2023-08-01","no_pmku":"PMKU.IDBXT.0822.000001","tgl_pmku":"2022-08-01 03:30:43","file_npwp":"\/v1\/doman\/download\/get?namafile=20220801032813.NPWP_PERUSAHAAN.pdf","file_siup":"\/v1\/doman\/download\/get?namafile=20220801032813.SURAT_IJIN_USAHA%280%29.pdf","file_organisasi":"\/v1\/doman\/download\/get?namafile=20220801032813.STRUKTUR_ORGANISASI_PT._LIB_NEW.pdf","file_domisili":"\/v1\/doman\/download\/get?namafile=20220801032813.domisili%280%29.pdf","file_siupkumham":"\/v1\/doman\/download\/get?namafile=20220801032813.siup_kum_ham_compressed.pdf"}}}
    // document.getElementById("beautified").innerHTML = JSON.stringify(data, undefined, 2);
</script>