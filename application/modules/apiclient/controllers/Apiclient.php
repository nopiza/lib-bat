<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apiclient extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'apiclient');

	public function __construct()
	{
		parent::__construct();
		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;

      	$this->load->view('template/header',$user_data);
      	$this->load->view('view',$user_data);

	}

	// ============================================================ authenticate ==============================================================>>>
	public function authenticate()
	{
      	$this->load->view('template/header');
      	$this->load->view('authenticate');
	}

	public function authenticate_response()
	{
		try {	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = $this->input->post('username');
			$object->password = $this->input->post('password');
			
			$results = $soapClient->authenticate($object);
			echo json_encode($results);
			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}

	// ============================================================ RPKRO ==============================================================>>>
	public function entryRpkro()
	{
      	$this->load->view('template/header');
      	$this->load->view('entryRpkro');
	}

	public function entryRpkro_response()
	{
		error_reporting(0);
		try {	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = $this->input->post('username');
			$object->password = $this->input->post('password');
			// Data
			$object->NomorRpkRo = $this->input->post('NomorRpkRo');
			$object->NomorLayanan = $this->input->post('NomorLayanan');
			$object->KodeDermaga = $this->input->post('KodeDermaga');
			$object->TanggalRencana = $this->input->post('TanggalRencana');
			$object->JamRencana = $this->input->post('JamRencana');
			// Data Detail
			$object->rpkroDetail->NomorPkk = $this->input->post('NomorPkk');
			$object->rpkroDetail->NomorPPKB = $this->input->post('NomorPPKB');
			$object->rpkroDetail->NomorRkbmMuat = $this->input->post('NomorRkbmMuat');
			$object->rpkroDetail->NomorRkbmBongkar = $this->input->post('NomorRkbmBongkar');
			$object->rpkroDetail->KegiatanBongkar = $this->input->post('KegiatanBongkar');
			$object->rpkroDetail->KegiatanMuat = $this->input->post('KegiatanMuat');
			$object->rpkroDetail->Komoditi = $this->input->post('Komoditi');
			$object->rpkroDetail->NomorGudang = $this->input->post('NomorGudang');
			$object->rpkroDetail->Keterangan = $this->input->post('Keterangan');
			$object->rpkroDetail->TanggalMulaiTambat = $this->input->post('TanggalMulaiTambat');
			$object->rpkroDetail->JamMulaiTambat = $this->input->post('JamMulaiTambat');
			$object->rpkroDetail->TanggalSelesaiTambat = $this->input->post('TanggalSelesaiTambat');
			$object->rpkroDetail->JamSelesaiTambat = $this->input->post('JamSelesaiTambat');
			$object->rpkroDetail->KadeMeterAwal = $this->input->post('KadeMeterAwal');
			$object->rpkroDetail->KadeMeterAkhir = $this->input->post('KadeMeterAkhir');

			$results = $soapClient->entryRpkro($object);
			echo json_encode($results);
			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}


	// ============================================================ SPK PANDU ==============================================================>>>
	public function SetSpkPandu()
	{
      	$this->load->view('template/header');
      	$this->load->view('SetSpkPandu');
	}


	public function SetSpkPandu_response()
	{
		try {	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = $this->input->post('username');
			$object->password = $this->input->post('password');
			// Data
			$object->NomorPKK = $this->input->post('NomorLayanan');
			$object->NomorPPK = $this->input->post('nomor_ppk');
			$object->NomorSPKPandu = $this->input->post('no_spk');
			$object->NamaPetugasPandu = $this->input->post('nama_petugas_pandu');
			$object->TanggalPandu = $this->input->post('pandu_tanggal');
			$object->JamPandu = $this->input->post('pandu_jam');
			$object->NamaKapalPandu1 = $this->input->post('kapal_pandu_1');
			$object->NamaKapalPandu2 = $this->input->post('NamaKapalPandu2');
			$object->NamaKapalTunda1 = $this->input->post('kapal_tunda_1');
			$object->NamaKapalTunda2 = $this->input->post('NamaKapalTunda2');
			$object->NamaKapalTunda3 = $this->input->post('NamaKapalTunda3');
			$object->NamaKapalTunda4 = $this->input->post('NamaKapalTunda4');
			$object->JenisPandu = $this->input->post('jenis_pandu');
			$object->LokasiAwal = $this->input->post('kode_dermaga_dari');
			$object->LokasiAkhir = $this->input->post('kode_dermaga_ke');
			$object->Kegiatan = $this->input->post('tujuan');
			$object->WaktuGerak = $this->input->post('waktu_gerak');


			$results = $soapClient->SetSpkPandu($object);
			echo json_encode($results);
			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}


	// ============================================================ Get entry Pkk ==============================================================>>>
	public function getEntryPKK()
	{
      	$this->load->view('template/header');
      	$this->load->view('getEntryPKK');
	}

	public function getEntryPKK_response()
	{
		try {	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = $this->input->post('username');
			$object->password = $this->input->post('password');
			// Data
			$object->nomorPKK = $this->input->post('nomorPKK');	

			$results = $soapClient->getEntryPKK($object);
			echo json_encode($results);
			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}

	// ============================================================ Get entry RKBM ==============================================================>>>
	public function getEntryRKBM()
	{
      	$this->load->view('template/header');
      	$this->load->view('getEntryRKBM');
	}

	public function getEntryRKBM_response()
	{
		try {	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = $this->input->post('username');
			$object->password = $this->input->post('password');
			// Data
			$object->nomorPKK = $this->input->post('nomorPKK');	

			$results = $soapClient->getEntryRKBM($object);
			echo json_encode($results);
			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}


	// ============================================================ Get entry RKBM ==============================================================>>>
	public function getEntrySPB()
	{
      	$this->load->view('template/header');
      	$this->load->view('getEntrySPB');
	}

	public function getEntrySPB_response()
	{
		try {	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = $this->input->post('username');
			$object->password = $this->input->post('password');
			// Data
			$object->nomorPKK = $this->input->post('nomorPKK');	

			$results = $soapClient->getEntrySPB($object);
			echo json_encode($results);
			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}


	public function tesrkbm()
	{
		error_reporting(0);
		try {	
			// $soapClient = new SoapClient('https://demo.lintasinternasionalberkarya.com/inaportnetbup.php?wsdl');
			$soapClient = new SoapClient('http://localhost/panda/api/demo/inaportnetbup.php?wsdl');
			
			$object = new stdClass();
			// $object->user = $this->input->post('username');
			// $object->password = $this->input->post('password');
			// Data
			$object->RequestEntryRKBM->nomorPKK = 'PKK.DN.IDBXT.2211.000010';
			$object->RequestEntryRKBM->portCode = 'IDBXT';
			// Data Detail
			$object->RequestEntryRKBM->DetailRKBM->namaBarang = 'BBM';
			$object->RequestEntryRKBM->DetailRKBM->bahaya = 'Y';
			$object->RequestEntryRKBM->DetailRKBM->ganggu = 'N';
			$object->RequestEntryRKBM->DetailRKBM->kegiatan = 'Bongkar';
			$object->RequestEntryRKBM->DetailRKBM->unit = '1';
			$object->RequestEntryRKBM->DetailRKBM->ton = '1';
			$object->RequestEntryRKBM->DetailRKBM->m3 = '1';
			$object->RequestEntryRKBM->DetailRKBM->penyaluran = 'CO';
			$object->RequestEntryRKBM->DetailRKBM->kade = 'IDBXT.T06.J06';
			$object->RequestEntryRKBM->DetailRKBM->pbm = 'PT CYBERINDO MEGATAMA';
			$object->RequestEntryRKBM->DetailRKBM->npwpPbm = '02.858.329.2-413.000';
			$object->RequestEntryRKBM->DetailRKBM->consignee = 'PT. A';
			$object->RequestEntryRKBM->DetailRKBM->shipper = 'PT. b';
			$object->RequestEntryRKBM->DetailRKBM->blNo = '1';
			$object->RequestEntryRKBM->DetailRKBM->npwpShipper = '12.342.435.2-313.121';
			$object->RequestEntryRKBM->DetailRKBM->gang = '1';
			$object->RequestEntryRKBM->DetailRKBM->palka = '1';
			$object->RequestEntryRKBM->DetailRKBM->noRkbmbongkar = 'SL004.IDBXT.1122.000005';
			$object->RequestEntryRKBM->DetailRKBM->noRkbmMuat = 'SL004.IDBXT.1122.000005';
			$object->RequestEntryRKBM->DetailRKBM->rencanaBongkar = '2022-11-29';
			$object->RequestEntryRKBM->DetailRKBM->rencanaMuat = '2022-11-29';
			$object->RequestEntryRKBM->DetailRKBM->insertId = '1';
			
			$results = $soapClient->entryRKBM($object);
			echo json_encode($results);
			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}































	

	public function ajax_delete($id)
	{
		$this->db->delete('origin',array('id_origin'=>$id));
		echo json_encode(array("status" => TRUE));
	}

}
