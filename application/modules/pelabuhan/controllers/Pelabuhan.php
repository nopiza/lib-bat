<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelabuhan extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'pelabuhan');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pelabuhan_model','pelabuhan');
		// $this->load->model('Group/Group_model','group');
		check_login();
	}

	public function index()
	{

		$user_data['data_ref'] = $this->data_ref;
		$user_data['title'] = 'Menu';

     	if($this->encryption->decrypt($this->session->userdata('is_admin')) == '1'){

	     	$this->load->view('template/header');
			$this->load->view('view',$user_data);
		}else{
			$user_data['alert'] = '0';
			$id = $this->encryption->decrypt($this->session->userdata('id'));
			$user_data['pelabuhan'] = $this->db->query("SELECT * FROM users WHERE id='$id'")->row_array();
			$this->load->view('template/header');
			$this->load->view('view_user',$user_data);
		}

	}

	public function ajax_list()
	{

		$list = $this->pelabuhan->get_datatables();
		$data = array();
		$no = $_POST['start'];


		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_pelabuhan."'".')"> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_pelabuhan."'".')"> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->kode_pelabuhan;
			$row[] = $post->nama_pelabuhan;
         	$row[] = $post->keterangan;

			if($post->stt_pelabuhan == '0'){
				$row[] = '<a href="'.base_url('pelabuhan/stt_pelabuhan/1/'.$post->id_pelabuhan).'" onclick="return confirm(\'Are you sure?\')">
				<span class="badge badge-pill badge-secondary">Non Aktif</span></a>';
			}else if($post->stt_pelabuhan == '1'){
				$row[] = '<a href="'.base_url('pelabuhan/stt_pelabuhan/0/'.$post->id_pelabuhan).'" onclick="return confirm(\'Are you sure?\')">
				<span class="badge badge-pill badge-success">Aktif</span></a>';
			}

			//add html for action
			$row[] = $link_edit.$link_hapus;
			$data[] = $row;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->pelabuhan->count_all(),
					"recordsFiltered" => $this->pelabuhan->count_filtered(),
					"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->pelabuhan->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		aktifitas('Login','Melakukan Penambahan pelabuhan');
		$data = array(
				'nama_pelabuhan' 		=> $this->input->post('nama_pelabuhan'),
				'kode_pelabuhan' 		=> $this->input->post('kode_pelabuhan'),
				'keterangan' 			=> $this->input->post('keterangan'),
				'stt_pelabuhan' 		=> '1'
		);

		$this->pelabuhan->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		aktifitas('Login','Melakukan Edit pelabuhan');
		$data = array(
			'nama_pelabuhan' 		=> $this->input->post('nama_pelabuhan'),
			'kode_pelabuhan' 		=> $this->input->post('kode_pelabuhan'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'stt_pelabuhan' 		=> '1'
	);
		
		$this->pelabuhan->update(array('id_pelabuhan' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function stt_pelabuhan($stt, $idPelabuhan)
	{
		// aktifitas('Login','Melakukan Hapus pelabuhan');
		$this->db->update('nama_pelabuhan', ['stt_pelabuhan'=> $stt],['id_pelabuhan' => $idPelabuhan]);
		redirect('pelabuhan');
	}

	public function ajax_delete($id)
	{
		aktifitas('Login','Melakukan Hapus pelabuhan');
		$this->db->delete('nama_pelabuhan', ['id_pelabuhan' => $id]);
		echo json_encode(array("status" => TRUE));
	}



}
