<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keagenan extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'keagenan');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Keagenan_model','keagenan');
		// $this->load->model('Group/Group_model','group');
		check_login();
	}

	public function index()
	{

		$user_data['data_ref'] = $this->data_ref;
		$user_data['title'] = 'Menu';
		
		$this->load->view('template/header');
		$this->load->view('view',$user_data);

//      	if($this->encryption->decrypt($this->session->userdata('is_admin')) == '1'){

// 	     	$this->load->view('template/header');
// 			$this->load->view('view',$user_data);
// 		}else{
// 			$user_data['alert'] = '0';
// 			$id = $this->encryption->decrypt($this->session->userdata('id'));
// 			$user_data['keagenan'] = $this->db->query("SELECT * FROM users WHERE id='$id'")->row_array();
// 			$this->load->view('template/header');
// 			$this->load->view('view_user',$user_data);
// 		}

	}

	public function ajax_list()
	{

		$list = $this->keagenan->get_datatables();
		$data = array();
		$no = $_POST['start'];


		foreach ($list as $post) {


				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';

				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';


			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_agen;
			$row[] = $post->nama_kepala;
         	$row[] = $post->alamat;
         	$row[] = $post->kontak;
			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->keagenan->count_all(),
						"recordsFiltered" => $this->keagenan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->keagenan->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		aktifitas('Login','Melakukan Penambahan keagenan');
		// $this->_validate();
		$post_date = time();
		$post_date_format = date('Y-m-d h:i:s', $post_date);
      // $user = $this->ion_auth->user()->row();
		$data = array(
				'nama_agen' 		=> $this->input->post('nama_agen'),
				'nama_singkat' 		=> $this->input->post('nama_singkat'),
				'nama_kepala' 		=> $this->input->post('nama_kepala'),
				'npwp' 			=> $this->input->post('npwp'),
            	'email' 		=> $this->input->post('email'),
            	'kontak' 		=> $this->input->post('kontak'),
            	'alamat' 		=> $this->input->post('alamat'),
				'username' 		=> $this->input->post('username'),
            	'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            	'is_trash' 		=> 0
		);

		$insert = $this->keagenan->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		aktifitas('Login','Melakukan Edit Data Keagenan');
		// $this->_validate();
		$post_date = time();
		$post_date_format = date('Y-m-d h:i:s', $post_date);

		if($this->input->post('password') == ''){
			$data = array(
				'nama_agen' 		=> $this->input->post('nama_agen'),
				'nama_singkat' 		=> $this->input->post('nama_singkat'),
				'nama_kepala' 		=> $this->input->post('nama_kepala'),
				'npwp' 			=> $this->input->post('npwp'),
            	'email' 		=> $this->input->post('email'),
            	'kontak' 		=> $this->input->post('kontak'),
            	'alamat' 		=> $this->input->post('alamat'),
				'username' 		=> $this->input->post('username')
			);
		}else{
			$data = array(
				'nama_agen' 		=> $this->input->post('nama_agen'),
				'nama_singkat' 		=> $this->input->post('nama_singkat'),
				'nama_kepala' 		=> $this->input->post('nama_kepala'),
				'npwp' 			=> $this->input->post('npwp'),
            	'email' 		=> $this->input->post('email'),
            	'kontak' 		=> $this->input->post('kontak'),
            	'alamat' 		=> $this->input->post('alamat'),
				'username' 		=> $this->input->post('username'),
            	'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT)
			);
		}
		
		
		$this->keagenan->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}


	public function update_user()
	{
		aktifitas('Login','Melakukan Update Data Agen');
		if($this->input->post('password') == ''){
			$data = array(
				'surname' 		=> $this->input->post('surname'),
				'username' 		=> $this->input->post('username'),

			);
		}else{
			$data = array(
				'surname' 		=> $this->input->post('surname'),
				'username' 		=> $this->input->post('username'),
            	'password' 		=> password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			);
		}
		
		
		$this->keagenan->update(array('id' => $this->input->post('id')), $data);
		// echo json_encode(array("status" => FALSE));
		// redirect('pengguna');
		$user_data['alert'] = '1';
		$id = $this->encryption->decrypt($this->session->userdata('id'));
		$user_data['keagenan'] = $this->db->query("SELECT * FROM users WHERE id='$id'")->row_array();
		$this->load->view('template/header');
		$this->load->view('view_user',$user_data);
	}

	public function ajax_delete($id)
	{
		aktifitas('Login','Melakukan Hapus Data keagenan');
		$this->keagenan->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('surname') == '')
		{
			$data['inputerror'][] = 'surname';
			$data['error_string'][] = 'Nama Lengkap harus diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('username') == '')
		{
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'username harus diisi';
			$data['status'] = FALSE;
		}

		// if($this->input->post('password') == '')
		// {
		// 	$data['inputerror'][] = 'password';
		// 	$data['error_string'][] = 'Password harus diisi';
		// 	$data['status'] = FALSE;
		// }

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}



}
