<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realisasi extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'realisasi');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Realisasi_model','realisasi');
		$this->load->model('Realisasi_tunda_model','realisasi_tunda');
		check_login();
	}

	public function index()
	{

	}

	public function pandu()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('view_realisasi_pandu',$user_data);
	}

	public function pandu_create()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('realisasi_pandu',$user_data);
	}


	public function ajax_list()
	{

		$list = $this->realisasi->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-success" href="'.base_url('realisasi/detailpandu/'.$post->id_realisasi_pandu).'"> View Detail</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->NomorPkk;
			$row[] = $post->nama_kapal;
			$row[] = $post->NomorPpkbPandu;
			$row[] = $post->NamaPandu;
			$row[] = $post->NomorSpk;
			$row[] = $post->JenisGerakan;
			//add html for action
			$row[] = $link_edit;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->realisasi->count_all(),
						"recordsFiltered" => $this->realisasi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->spkpandu->get_by_id($id);
		echo json_encode($data);
	}

	public function save_pandu()
	{
		$panduNaik 		= $this->input->post('pandu_naik');
		$naik = explode('T',$panduNaik);
		$panduMulai 	= $this->input->post('pandu_mulai');
		$mulai = explode('T',$panduMulai);
		$panduSelesai 	= $this->input->post('pandu_selesai');
		$selesai = explode('T',$panduSelesai);
		$panduTurun 	= $this->input->post('pandu_turun');
		$turun = explode('T',$panduTurun);
	
		$param = [
			'NomorPkk'				=> $this->input->post('NomorLayanan'),
			'NomorSpk'				=> $this->input->post('no_spk'),										
			'TanggalMulaiPandu'		=> $mulai[0],		
			'JamMulaiPandu'			=> $mulai[1],		
			'TanggalNaikPandu'		=> $naik[0],	
			'JamNaikPandu'			=> $naik[1],		
			'TanggalSelesaiPandu'	=> $selesai[0],		
			'JamSelesaiPandu'		=> $selesai[1],		
			'TanggalTurunPandu'		=> $turun[0],		
			'JamTurunPandu'			=> $turun[1],		
			'JenisPandu'			=> $this->input->post('jenis_pandu'),		
			'JenisGerakan'			=> $this->input->post('tujuan'),		
			'LokasiAwal'			=> $this->input->post('kode_dermaga_dari'),		
			'LokasiAkhir'			=> $this->input->post('kode_dermaga_ke'),		
			'GtKapal'				=> $this->input->post('gt'),		
			'TarifTetap'			=> $this->input->post('biaya_tetap'),		
			'TarifTVariabel'		=> $this->input->post('biaya_variabel'),		
			'Biaya'					=> $this->input->post('alasan'),		
			'NamaPandu' 			=> $this->input->post('nama_petugas_pandu')
		];
		$this->db->insert('realisasi_pandu', $param);
		redirect('realisasi/pandu');
	}

	public function ajax_delete($id)
	{
		$this->db->delete('ppk',array('id_pkk'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	// REalisasi TUNDA ============================================================================================>>>>
	public function tunda()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('view_realisasi_tunda',$user_data);
	}

	public function tunda_create()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('realisasi_tunda',$user_data);
	}

	public function ajax_list_tunda()
	{

		$list = $this->realisasi_tunda->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-success" href="'.base_url('realisasi/detailtunda/'.$post->id_realisasi_tunda).'"> View Detail</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nomor_pkk.' - ';
			$row[] = $post->nama_kapal;
			$row[] = $post->NomorPpkbTunda;
			$row[] = $post->JenisKegiatan;
			$row[] = '';
			$row[] = '';
			//add html for action
			$row[] = $link_edit;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->realisasi_tunda->count_all(),
						"recordsFiltered" => $this->realisasi_tunda->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function save_tunda()
	{
		$Mulai 	= $this->input->post('mulai');
		$mulai = explode('T',$Mulai);
		$Selesai 	= $this->input->post('selesai');
		$selesai = explode('T',$Selesai);

	
		$param = [
			'NomorPpk'				=> $this->input->post('NomorLayanan'),	
			'JenisKegiatan'			=> $this->input->post('tujuan'),		
			'NomorPpkbTunda'		=> $this->input->post('no_ppkb_tunda'),	
			'TanggalMulaiTunda'		=> $mulai[0],		
			'JamMulaiTunda'			=> $mulai[1],	
			'TanggalMulaiTunda'		=> $selesai[0],		
			'JamMulaiTunda'			=> $selesai[1],		
			'LokasiAwal'			=> $this->input->post('kode_dermaga_dari'),		
			'LokasiAkhir'			=> $this->input->post('kode_dermaga_ke'),	
			'GtKapal'				=> $this->input->post('gt'),	
			'TarifTetap' 			=> $this->input->post('biaya_tetap'),	
			'TarifTVariabel' 		=> $this->input->post('biaya_variabel'),
			'Prosentase' 			=> $this->input->post('prosentase'),
			'Biaya' 				=> $this->input->post('biaya'),		
			'DurasiTunda' 			=> '0'
		];
		$this->db->insert('realisasi_tunda', $param);
		redirect('realisasi/tunda');
	}



	//########################################################################################>>>
	function get_ppk($noPKK){
		// cari no RPKRO di tabel _rpkro
		$item = $this->db->query("SELECT * FROM pkk WHERE nomor_pkk = '$noPKK'")->row_array();
		$bendera = $item['bendera'];

		// cari tarif
		$tarif = $this->db->query("SELECT * FROM tarif WHERE jenis='PANDU'")->row_array();
		$item['tarifTetapLokal'] = $tarif['tarif_tetap_lokal'];
		$item['tarifVariabelLokal'] = $tarif['tarif_variabel_lokal'];
		// $no = 'IDBXT-'.$rpkro['NomorRpkRo'];
		// $ppk = $this->db->query("SELECT * FROM ppk WHERE nomor_rpkro = '$no'")->row_array();
		return $this->output->set_content_type('application/json')->set_output(json_encode($item));      
	}

}
