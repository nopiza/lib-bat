<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apiserver extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'origin');

	public function __construct()
	{
		parent::__construct();
		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('view',$user_data);
	}

	public function pkk()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('detail_pkk',$user_data);
	}



}
