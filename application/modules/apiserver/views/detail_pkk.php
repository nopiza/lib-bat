  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Detail PKK</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data PKK</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data PKK</h3>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="20%">No. PKK</th>
                        <th width="20%">Nama Perusahaan</th>
                        <th width="15%">Nama Kapal</th>
                        <th width="15%">Tanggal Insert</th>
                        <th width="10%">Status</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $no =1;
                    $data_pkk = $this->db->query("SELECT * FROM pkk ORDER BY tgl_data DESC")->result(); 
                    foreach($data_pkk as $pkk){
                        echo '<tr>
                        <td>'.$no++.'</td>
                        <td>'.$pkk->nomor_pkk.'</td>
                        <td>'.$pkk->nama_perusahaan.'</td>
                        <td>'.$pkk->nama_kapal.'</td>
                        <td>'.$pkk->tgl_data.'</td>
                        <td>'.$pkk->stt_proses.'</td>
                        <td>Detail</td>
                    </tr>';
                    }
                    ?>
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>