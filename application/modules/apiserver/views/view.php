  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Proses Soap Server</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Proses Server</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">List Enpoint</h3>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

           <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="50%">Nama Endpoint</th>
                        <th width="15%">Jumlah Data</th>
                        <th width="15%">Belum Dilihat / Diproses</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM pkk")->row_array(); ?>
                    <tr>
                        <td>1</td>
                        <td>entryPKK</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM rkbm")->row_array(); ?>
                    <tr>
                        <td>2</td>
                        <td>entryRKBM</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM ppk")->row_array(); ?>
                    <tr>
                        <td>3</td>
                        <td>entryPPK</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM spog")->row_array(); ?>
                    <tr>
                        <td>4</td>
                        <td>entrySPOG</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM spb")->row_array(); ?>
                    <tr>
                        <td>5</td>
                        <td>entrySPB</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id_ijingerak) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM ijin_gerak")->row_array(); ?>
                    <tr>
                        <td>6</td>
                        <td>entryIjinGerak</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM pindah_keluar")->row_array(); ?>
                    <tr>
                        <td>7</td>
                        <td>entryPindahKeluar</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM perpanjangan_masa_tambat")->row_array(); ?>
                    <tr>
                        <td>8</td>
                        <td>entryPerpanjanganMasaTambat</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM pembatalan_pkk")->row_array(); ?>
                    <tr>
                        <td>9</td>
                        <td>entryPembatalanPKK</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM pembatalan_spog")->row_array(); ?>
                    <tr>
                        <td>10</td>
                        <td>entryPembatalanSPOG</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM pembatalan_ppk")->row_array(); ?>
                    <tr>
                        <td>11</td>
                        <td>entryPembatalanPPK</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>

                    <?php $data_pkk = $this->db->query("SELECT *, count(id) as jum, (SELECT COUNT(stt_proses='0')) as jumpros FROM pembatalan_spb")->row_array(); ?>
                    <tr>
                        <td>12</td>
                        <td>entryPembatalanSPB</td>
                        <td><?=$data_pkk['jum'];?></td>
                        <td><?=$data_pkk['jumpros'];?></td>
                        <td><a href="<?=base_url('apiserver/pkk');?>" class="btn btn-sm btn-info">Detail Data</a></td>
                    </tr>
                </tbody>
            </table>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>