<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pranota extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'rpkro');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Rpkro_model','rpkro');
		check_login();
	}

	public function index()
	{
		// echo penomoran('RPKRO MASUK', 'LN');
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('view',$user_data);
	}

	public function create()
	{
      	$user_data['data_ref'] = $this->data_ref;
		$user_data['pelabuhan'] = $this->db->get_where('nama_pelabuhan', ['stt_pelabuhan' => '1'])->result();
      	$this->load->view('template/header',$user_data);
      	$this->load->view('create',$user_data);
	}

	public function detail($id)
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$user_data['rpkro'] = $this->db->query("SELECT * FROM z_simpan_rpkro WHERE id_simpan_rpkro='$id'")->row_array();
      	$this->load->view('template/header',$user_data);
      	$this->load->view('detail',$user_data);
	}

	

	function get_ppk($nopkk){
		$data =  $this->db->query("SELECT * FROM z_simpan_rpkro WHERE NomorLayanan='$nopkk'")->result();
		$a = '<table class="table table-bordered">
		<thead>
		  <tr>
			<th width="3%">#</th>
			<th width="20%">No. RPKRO</th>
			<th width="15%">Gerakan / SPK</th>
			<th width="20%">Kode Dermaga</th>
			<th width="5%">Biaya Pandu</th>
			<th width="5%">Biaya Tunda</th>
			<th width="20%">Tarif</th>
		  </tr>
		</thead>
		<tbody>';
		$no =1;
		foreach($data as $dt){
			$a .= ' <tr>
			<th scope="row">'.$no++.'</th>
			<td>'.$dt->NomorRpkRo.'</td>
			<td>'.$dt->jenis.'</td>
			<td>'.$dt->KodeDermaga.'</td>
			<td>
				<div class="form-group form-check">
					<input type="checkbox" class="form-check-input" id="exampleCheck1">
				</div>
  			</td>
			<td>
			<div class="form-group form-check">
				<input type="checkbox" class="form-check-input" id="exampleCheck1">
			</div>
			</td>
			<td>
				<select>
					<option value="">-- pilih jenis tarif --</option>
					<option value="">Tarif GRT</option>
					<option value="">Tarif FLAT</option>
				</select>
			</td>
		  </tr>';
		}
		 
		  
		$a .= '</tbody>
	  </table>';

	  $a .= '<button class="btn btn-sm btn-primary" type="submit">Proses Pranota</button>';

		echo json_encode($a);
	}
	



}
