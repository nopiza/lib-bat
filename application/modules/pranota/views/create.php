  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Rencana Pelayanan Kapal dan Rencana Operasi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
              <li class="breadcrumb-item active">RPKRO</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">


        <!-- /.card-header -->
        <div class="card-body">

        <form method="POST" action="<?=base_url('rpkro/save');?>" accept-charset="UTF-8" enctype="multipart/form-data"><input name="_token" type="hidden" value="Pr2K3a6ho0JIY6ziNEjDI9vAoUP4de9ZNOg0mCMd">
            <div class="card custom-card">
                <div class="card-header">
                <h3 class="card-title">RPKRO </h3>
                    <div class="card-tools">
                        <a href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Singkron data RPKRO Agen</a>&nbsp;
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Nomor RPKRO</b></label>
                            <div class="input-group">
                                <input class="form-control" id="NomorRpkRo" name="NomorRpkRo" type="text" readonly>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jenis RPKRO</b></label>
                            <select class="form-control" required id="jenis_rpkro" name="jenis_rpkro">
                                <option value="">-- pilih --</option>
                                <option value="MASUK">Masuk</option>
                                <option value="PINDAH">Pindah</option>
                                <option value="PERPANJANGAN">Perpanjangan</option>
                            </select>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Nomor Layanan / Produk</b></label>
                            <select class="form-control" id="NomorLayanan" required name="NomorLayanan">
                                <option value="">-- pilih --</option>
                                <div id="pkk"></div>
                                </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Lokasi Sandar / Labuh</b></label>
                            <select class="form-control select2" id="lokasi_sandar_labuh" required name="lokasi_sandar_labuh">
                                <?php 
                                foreach($pelabuhan as $plb){
                                    echo '<option value="'.$plb->kode_pelabuhan.'">'.$plb->nama_pelabuhan.'</option>';
                                }
                                ?>
                            </select>
                            <input class="form-control" readonly id="id-ppk" name="id" type="hidden">
                            <input class="form-control" readonly id="dermaga-kode" name="dermaga_kode" type="hidden">
                            <input class="form-control" readonly id="pkk-nomor" name="pkk_nomor" type="hidden">
                            <input class="form-control" readonly id="loa" name="loa" type="hidden">
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Tanggal Rencana</b></label>
                            <input class="form-control" required name="TanggalRencana" type="date" id="TanggalRencana">
                        </div>
                        
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jam Rencana</b></label>
                            <input class="form-control" required name="JamRencana" type="time">
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Gambar Tambatan</b></label>
                            <input class="form-control" id="gambar-tambatan" name="gambar_tambatan" type="file">
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Keterangan</b></label>
                            <input class="form-control" name="keterangan" type="text">
                        </div>
                    </div>
                </div>


                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-1"><b>Detail RPKRO</b></h6>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Nomor RKBM</b></label>
                            <!-- <input class="form-control" id="NomorRkbmMuat" name="NomorRkbmMuat" type="text"> -->
                            <select class="form-control select2" id="NomorRkbmMuat" name="NomorRkbmMuat">
                                    <option selected="selected" value=""> -- Pilih --</option>
                            </select>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Nomor PPKB</b></label>
                            <div class="input-group">
                                <input class="form-control" id="NomorPPKB" name="NomorPPKB" type="text" readonly>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Komoditi</b></label>
                            <input class="form-control" placeholder="Komoditi" id="Komoditi" name="Komoditi" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Bongkar</b></label>
                            <div class="input-group">
                                <input class="form-control" placeholder="Bongkar" id="kegiatan-bongkar" name="kegiatan_bongkar" type="text">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Ton</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Muat</b></label>
                            <div class="input-group">
                                <input class="form-control" placeholder="Muat" id="kegiatan-muat" name="kegiatan_muat" type="text">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Ton</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Tanggal Mulai</b></label>
                            <div class="input-group">
                                <input class="form-control" required placeholder="Tanggal Mulai" id="TanggalMulaiTambat" name="TanggalMulaiTambat" type="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jam Mulai</b></label>
                            <div class="input-group">
                                <input class="form-control" required placeholder="Jam Mulai" id="JamMulaiTambat" name="JamMulaiTambat" type="time">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Tanggal Selesai</b></label>
                            <div class="input-group">
                                <input class="form-control" required placeholder="Tanggal Selesai" id="TanggalSelesaiTambat" name="TanggalSelesaiTambat" type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jam Selesai</b></label>
                            <div class="input-group">
                                <input class="form-control" placeholder="Jam Selesai" id="JamSelesaiTambat" name="JamSelesaiTambat" type="time">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Kade Meter Awal</b></label>
                            <div class="input-group">
                                <input class="form-control" onBlur="kadeAkhir()" required placeholder="Kade Meter Awal" id="KadeMeterAwal" name="KadeMeterAwal" type="text" value="0">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Kade Meter Akhir</b></label>
                            <div class="input-group">
                                <input class="form-control" required placeholder="Kade Meter Akhir" id="KadeMeterAkhir" name="KadeMeterAkhir" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="card-footer">
                    <div class="row" style="float:right;">
                        <div class="btn-box">
                        <button class="btn btn-md btn-primary fw-bold rounded-2 height-50px" type="submit">Simpan</button>
                        <a href="<?=base_url('rpkro');?>" class="btn btn-md btn-secondary fw-bold rounded-2 height-50px"> Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
            </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>

<?php  $this->load->view('template/footer'); ?>\
<script>
var url_apps = "<?=base_url();?>"

$('#jenis_rpkro').on('change', function(){
    var jenis_rpkro = $(this).val();
    $("#NomorLayanan").load('<?php echo base_url('rpkro/get_ppk/');?>'+ jenis_rpkro);
});

// $('#NomorLayanan').on('change', function() {

//         // alert('asd');
//         var rpkro = document.getElementById("NomorLayanan").value;
//         // $("#NomorRkbmMuat").load('<?php echo base_url('rpkro/get_ppk/');?>');
//         $("#NomorRkbmMuat").load('<?php echo base_url('rpkro/getEntryRKBM_response/');?>'+ rpkro);

// });


$('#NomorLayanan').on('change', function() {
  var jenis = document.getElementById("jenis_rpkro").value;
  $.ajax({
    url: url_apps + 'rpkro/get_nomor/' + $(this).val() + '/' + jenis,
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    // $("#nama_barang_real").val(data.nama_barang);
    // alert(data);
    $("#NomorRpkRo").val('IDBXT-' + data.rpkro);
    $("#NomorPPKB").val('IDBXT-' + data.ppkb);
    
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

  $("#NomorRkbmMuat").load('<?php echo base_url('rpkro/get_rkbm/');?>'+ $(this).val());
});


// mencari RPKRO dari agen ==============================================>
$('#NomorLayanan').on('change', function() {
  var jenis = document.getElementById("jenis_rpkro").value;
  $.ajax({
    url: url_apps + 'rpkro/get_rpkro_agen/' + $(this).val() + '/' + jenis,
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    if(data.stt_pkk_agen){
        $("#TanggalRencana").val(data.arrival_time);
        $('[name="JamRencana"]').val(data.arrival_jam);
        $('[name="TanggalMulaiTambat"]').val(data.tanggal_mulai);
        $('[name="JamMulaiTambat"]').val(data.jam_mulai);
        $('[name="TanggalSelesaiTambat"]').val(data.tanggal_selesai);
        $('[name="JamSelesaiTambat"]').val(data.jam_selesai);
        $('[name="lokasi_sandar_labuh"]').val(data.lokasi_tujuan);
        
    }else{
        alert('Tidak ada data RPKRO dari agen');
    }
    
    
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

  $("#NomorRkbmMuat").load('<?php echo base_url('rpkro/get_rkbm/');?>'+ $(this).val());
});
</script>