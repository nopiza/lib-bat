  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Penghitungan Pranota</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Pranota</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
              <div class="card-tools">
                <a href="<?=base_url('pranota/masterdata');?>" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Data Pranota</a>&nbsp;
                <button class="btn btn-default btn-sm" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

        <form method="POST" action="<?=base_url('pranota/proses');?>" accept-charset="UTF-8" enctype="multipart/form-data">

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nomor Layanan</label>
    <div class="col-sm-5">
            <select class="form-control" id="NomorLayanan" required name="NomorLayanan">
                    <option value="">-- pilih --</option>
                    <?php 
                    $layanan = $this->db->query("SELECT nomor_pkk, nama_kapal FROM pkk")->result();
                    foreach($layanan as $ly){
                        echo '<option value="'.$ly->nomor_pkk.'">'.$ly->nama_kapal.' # '.$ly->nomor_pkk.'</option>';
                    }
                    ?>
                    </select>
    </div>
  </div>


                    <div id="detail"></div>

                    </form>
          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>





<?php  $this->load->view('template/footer'); ?>


<script type="text/javascript">

$('#NomorLayanan').on('change', function() {
        $.ajax({
            url: url_apps + 'pranota/get_ppk/' + $(this).val(),
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            document.getElementById('detail').innerHTML = data;
            // $("#nama_barang_real").val(data.nama_barang);
            // alert(data.nomor_ppk);
            // $("#ppk-nomor").val(data.nomor_ppk);
            // $("#nomor-spk").val(data.noSPKPandu);
            // $("#NomorPPKB").val(data.ppkb);
            
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        $("#NomorRkbmMuat").load('<?php echo base_url('rpkro/get_rkbm/');?>'+ $(this).val());
    });


</script>


