<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rpkro extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'rpkro');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Rpkro_model','rpkro');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
		// echo penomoran('RPKRO MASUK', 'LN');
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('view',$user_data);
	}

	public function create()
	{
      	$user_data['data_ref'] = $this->data_ref;
		$user_data['pelabuhan'] = $this->db->get_where('nama_pelabuhan', ['stt_pelabuhan' => '1'])->result();
      	$this->load->view('template/header',$user_data);
      	$this->load->view('create',$user_data);
	}

	public function detail($id)
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$user_data['rpkro'] = $this->db->query("SELECT * FROM z_simpan_rpkro WHERE id_simpan_rpkro='$id'")->row_array();
      	$this->load->view('template/header',$user_data);
      	$this->load->view('detail',$user_data);
	}

	public function ajax_list()
	{

		$list = $this->rpkro->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a href="'.base_url('rpkro/detail/'.$post->id_simpan_rpkro).'" class="btn btn-xs btn-secondary"> View Detail</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->NomorPkk;
			$row[] = $post->nama_kapal;
			$row[] = $post->NomorRpkRo;
			$row[] = '';
			$row[] = '';
			$row[] = '';
			$row[] = '';
			//add html for action
			$row[] = $link_edit;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->rpkro->count_all(),
						"recordsFiltered" => $this->rpkro->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function save()
	{

		$NomorRkbmMuat = $this->input->post('NomorRkbmMuat');
		$rkbm = $this->db->get_where('detail_rkbm',array('id' => $NomorRkbmMuat))->row_array();
		// die(var_dump($this->input->post()));
		// echo json_encode(array("status" => TRUE));
		error_reporting(0);
		$res = '';
		try {	
	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = 'lintas-bontang';
			$object->password = '23Okto1991';
			// Data
			$object->NomorRpkRo = $this->input->post('NomorRpkRo');
			$object->NomorLayanan = $this->input->post('NomorLayanan');
			$object->KodeDermaga = $this->input->post('lokasi_sandar_labuh');
			$object->TanggalRencana = $this->input->post('TanggalRencana');
			$object->JamRencana = $this->input->post('JamRencana');
			// Data Detail
			$object->rpkroDetail->NomorPkk = $this->input->post('NomorLayanan');
			$object->rpkroDetail->NomorPPKB = $this->input->post('NomorPPKB');
			$object->rpkroDetail->NomorRkbmMuat = 		$rkbm['no_rkbm_muat'];
			$object->rpkroDetail->NomorRkbmBongkar = 	$rkbm['no_rkbm_bongkar'];
			$object->rpkroDetail->KegiatanBongkar = $this->input->post('KegiatanBongkar');
			$object->rpkroDetail->KegiatanMuat = $this->input->post('KegiatanMuat');
			$object->rpkroDetail->Komoditi = $this->input->post('Komoditi');
			$object->rpkroDetail->NomorGudang = $this->input->post('NomorGudang');
			$object->rpkroDetail->Keterangan = $this->input->post('Keterangan');
			$object->rpkroDetail->TanggalMulaiTambat = $this->input->post('TanggalMulaiTambat');
			$object->rpkroDetail->JamMulaiTambat = $this->input->post('JamMulaiTambat');
			$object->rpkroDetail->TanggalSelesaiTambat = $this->input->post('TanggalSelesaiTambat');
			$object->rpkroDetail->JamSelesaiTambat = $this->input->post('JamSelesaiTambat');
			$object->rpkroDetail->KadeMeterAwal = $this->input->post('KadeMeterAwal');
			$object->rpkroDetail->KadeMeterAkhir = $this->input->post('KadeMeterAkhir');

			$results = $soapClient->entryRpkro($object);
			$res = json_encode($results);
			// die(var_dump($results->entryRpkroResult->return));
			if($results->entryRpkroResult->return->statusCode!='01'){
				die($results->entryRpkroResult->return->statusMessage);
			}

			$param = array(
				'NomorRpkRo'			=> $this->input->post('NomorRpkRo'),
				'NomorLayanan'			=> $this->input->post('NomorLayanan'),
				'KodeDermaga'			=> $this->input->post('lokasi_sandar_labuh'),
				'jenis'					=> $this->input->post('jenis_rpkro'),
				'TanggalRencana'		=> $this->input->post('TanggalRencana'),
				'JamRencana'			=> $this->input->post('JamRencana'),
				'NomorPkk'				=> $this->input->post('NomorLayanan'),
				'NomorPPKB'				=> 'IDBXT-'.$this->input->post('NomorPPKB'),
				'NomorRkbmMuat'			=> $rkbm['no_rkbm_muat'],
				'NomorRkbmBongkar'		=> $rkbm['no_rkbm_bongkar'],
				'KegiatanBongkar'		=> '',
				'KegiatanMuat'			=> '',
				'Komoditi'				=> $this->input->post('Komoditi'),
				'NomorGudang'			=> '',
				'Keterangan'			=> $this->input->post('keterangan'),
				'TanggalMulaiTambat'	=> $this->input->post('TanggalMulaiTambat'),
				'JamMulaiTambat'		=> $this->input->post('JamMulaiTambat'),
				'TanggalSelesaiTambat'	=> $this->input->post('TanggalSelesaiTambat'),
				'JamSelesaiTambat'		=> $this->input->post('JamSelesaiTambat'),
				'KadeMeterAwal'			=> $this->input->post('KadeMeterAwal'),
				'KadeMeterAkhir'		=> $this->input->post('KadeMeterAkhir'),
				'id_detail_rkbm'		=> $this->input->post('NomorRkbmMuat'),
				'status_rpkro'			=> '0',
				'json_respon'			=> $res
			);
			$this->db->insert('z_simpan_rpkro', $param);
			$this->db->update('pkk',array('stt_proses' => 2),array('nomor_pkk' => $this->input->post('NomorLayanan')));

			// Simpan Penomoran
			// Nomor RPKRO
			$blthnRPKRO = explode('-', $this->input->post('NomorRpkRo'));
			$paramRPKRO = [
				'nomor_surat'		=> str_replace('IDBXT-', '', $this->input->post('NomorRpkRo')),
				'jenis_kegiatan'	=> 'RPKRO '.$this->input->post('jenis_rpkro'),
				'bln_thn'			=> $blthnRPKRO[5],
				'id_kegiatan'		=> '1',
				'keterangan'		=> ''
			];
			$this->db->insert('penomoran', $paramRPKRO);
			// Nomor PPKB
			$blthnPPKB = explode('-', $this->input->post('NomorPPKB'));
			$paramPPKB = [
				'nomor_surat'		=> str_replace('IDBXT-', '', $this->input->post('NomorPPKB')),
				'jenis_kegiatan'	=> 'PPKB '.$this->input->post('jenis_rpkro'),
				'bln_thn'			=> $blthnPPKB[5],
				'id_kegiatan'		=> '1',
				'keterangan'		=> ''
			];
			$this->db->insert('penomoran', $paramPPKB);

			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
		// $this->db->update('detail_rkbm',array('stt_proses' => 1),array('id' => $rkbm['id']));
		// die(var_dump($res));
		redirect('rpkro');
	}


	// public function nomor(){

	// 	$NamaBUP = 'LIB'; //Nama BUP
	// 	$EUP = 'EUP'; //Jenis Surat
	// 	$Jkapal = 'DN';
	// 	$blnThn = date('my');
	// 	$nsurat = $this->db->query("SELECT * FROM penomoran WHERE jenis_kegiatan = 'RPKRO MASUK' AND jenis_kapal = '$Jkapal' AND bln_thn = '$blnThn'")->row_array();
	// 	$cek = $this->db->query("SELECT * FROM penomoran WHERE jenis_kegiatan = 'RPKRO MASUK' AND jenis_kapal = '$Jkapal' AND bln_thn = '$blnThn'")->num_rows();

	// 	if($cek){
	// 		$nomorDB = $nsurat['nomor_surat'];
	// 		$pecah = explode('-', $nomorDB);
	// 		$NamaBUP = $pecah[0]; //Nama BUP
	// 		$EUP = $pecah[1]; //Jenis Surat
	// 		$Jkapal = $pecah[2]; //Jenis Kapal
	// 		$blnThn = $pecah[3]; //Bulan Tahun
	// 		$nmrF = $pecah[4]+1; //Nomor
	// 		$nmr = sprintf("%05s", $nmrF);
	// 	}else{
	// 		$nmr = '00001';
	// 	}

	// 	$nomorSekarang = $NamaBUP.'-'.$EUP.'-'.$Jkapal.'-'.$blnThn.'-'.$nmr;
	// 	echo $nomorSekarang;

	// }

	function get_ppk($status){
		$a = '<option value="">-- pilih --</option>';
		if($status=='MASUK')$where = ' where stt_proses = 1'; else $where = ' where stt_proses = 2';
		$layanan = $this->db->query("SELECT nomor_pkk, nama_kapal FROM pkk $where ")->result();
		foreach($layanan as $ly){
			$a .= '<option value="'.$ly->nomor_pkk.'">'.$ly->nama_kapal.' # '.$ly->nomor_pkk.'</option>';
		}
		echo $a;
	}

	function get_rpkro_agen($no_pkk, $jenis){
		$query = "SELECT * FROM agen_rpkro WHERE no_pkk='$no_pkk' AND jenis_pergerakan='$jenis'";
		$cek = $this->db->query($query)->num_rows();
		if($cek){
			$rpkroAgen = $this->db->query($query)->row_array();
		}
		$rpkroAgen['stt_pkk_agen'] = $cek;
		echo json_encode($rpkroAgen);
	}

	function get_rkbm($no_pkk){
		$a = '<option value="">-- pilih --</option>';
		// if($status=='MASUK')$where = ' where stt_proses = 0'; else $where = ' where stt_proses = 1';
		$layanan = $this->db->query("SELECT * FROM detail_rkbm where nomor_pkk='".$no_pkk."' ")->result();
		foreach($layanan as $ly){
			$a .= '<option value="'.$ly->id.'">No RKBM Bongkar '.$ly->no_rkbm_bongkar.' # No RKBM Muat '.$ly->no_rkbm_muat.'</option>';
		}
		echo $a;
	}

	public function get_nomor($id_pkk, $jenis){
		$kegiatan = 'RPKRO '.strtoupper($jenis);
        // $rpkro = $this->db->query("SELECT MAX(nomor_surat) as besar FROM penomoran WHERE jenis_kegiatan = '$kegiatan'")->row_array();

		$kegiatan2 = 'PPKB '.strtoupper($jenis);
        // $ppkb = $this->db->query("SELECT MAX(nomor_surat) as besar FROM penomoran WHERE jenis_kegiatan = '$kegiatan2'")->row_array();
        //output to json format
		$items['rpkro'] = penomoran($kegiatan, 'DN');
		$items['ppkb'] = penomoran($kegiatan2, 'KM');
        echo json_encode($items);
    }


	public function getEntryRKBM_response($noPKK)
	{
		try {	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = 'lintas-bontang';
			$object->password = '23Okto1991';
			// Data
			$object->nomorPKK = $noPKK;	
			$results = $soapClient->getEntryRKBM($object);
			echo json_encode($results);
			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}

	



}
