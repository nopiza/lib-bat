  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Rencana Pelayanan Kapal dan Rencana Operasi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
              <li class="breadcrumb-item active">RPKRO</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">


        <!-- /.card-header -->
        <div class="card-body">

        <form method="POST" action="<?=base_url('rpkro/save');?>" accept-charset="UTF-8" enctype="multipart/form-data"><input name="_token" type="hidden" value="Pr2K3a6ho0JIY6ziNEjDI9vAoUP4de9ZNOg0mCMd">
            <div class="card custom-card">
                <div class="card-header">
                    <div>
                        <h6 class="main-content-label mb-1"><b>RPKRO</b></h6>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Nomor RPKRO</b></label>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon1">IDBXT-LIB-</span>
                                </div>
                                <input class="form-control" id="NomorRpkRo" name="NomorRpkRo" type="text" value="<?=$rpkro['NomorRpkRo'];?>" readonly>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jenis RPKRO</b></label>
                            <select class="form-control" required id="jenis_rpkro" name="jenis_rpkro">
                                <option value=""><?=$rpkro['jenis'];?></option>
                            </select>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Nomor Layanan / Produk</b></label>
                            <select class="form-control" id="NomorLayanan" required name="NomorLayanan">
                                <option value=""><?=$rpkro['NomorLayanan'];?></option>
                                </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Lokasi Sandar / Labuh</b></label>
                            <select class="form-control select2" id="lokasi_sandar_labuh" required name="lokasi_sandar_labuh">
                                <option value="IDBXT.T01.J01">TERSUS PT. INDOMINCO MANDIRI DERMAGA I</option>
                                <option value="IDBXT.T01.J02">TERSUS PT. INDOMINCO MANDIRI DERMAGA II</option>
                                <option value="IDBXT.T01.J03">TERSUS PT. INDOMINCO MANDIRI DERMAGA III</option>
                                <option value="IDBXT.T01.M01">TERSUS PT. INDOMINCO MANDIRI DERMAGA IV</option>
                                <option value="IDBXT.T01.S01">TERSUS PT. INDOMINCO MANDIRI FTS</option>
                                <option value="IDBXT.T02.J04">TUKS PT. GRAHA POWER KALTIM</option>
                                <option value="IDBXT.T03.J05">TUKS PT. PERTAMINA DERMAGA I</option>
                                <option value="IDBXT.T03.J06">TUKS PT. PERTAMINA DERMAGA II</option>
                                <option value="IDBXT.T03.J07">TUKS PT. PERTAMINA DERMAGA III</option>
                                <option value="IDBXT.T03.M02">TUKS PT. PERTAMINA DERMAGA CARGO</option>
                                <option value="IDBXT.T03.D01">TUKS PT. PERTAMINA DERMAGA DOCKING</option>
                                <option value="IDBXT.T04.M03">TUKS PT. HARLIS TATA TAHTA</option>
                                <option value="IDBXT.T06.J05">PELABUHAN UMUM TANJUNG LAUT</option>
                                <option value="IDBXT.T06.J06">PELABUHAN UMUM LOK TUAN DERMAGA I</option>
                                <option value="IDBXT.T06.J07">PELABUHAN UMUM LOK TUAN DERMAGA II</option>
                                <option value="IDBXT.T06.J08">PELABUHAN UMUM LOK TUAN DERMAGA III</option>
                                <option value="IDBXT.T07.M04">TERSUS PT. BLACK BEAR RESOURCES INDONESIA</option>
                                <option value="IDBXT.T08.O01">TERSUS PT. KALTIM METHANOL INDUSTRI</option>
                                <option value="IDBXT.T09.MU05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I UTARA</option>                                
                                <option value="IDBXT.T09.MS05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I SELATAN</option>
                                <option value="IDBXT.T09.M06">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA II</option>
                                <option value="IDBXT.T09.O02">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA III</option>
                                <option value="IDBXT.T09.M07">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA IV</option>
                                <option value="IDBXT.T09.O03">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VI</option>
                                <option value="IDBXT.T09.D01">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VII</option>
                                <option value="IDBXT.T09.O04">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VIII</option>
                                <option value="IDBXT.A01">AREA LABUH SHIP TO SHIP</option>
                                <option value="IDBXT.A02">AREA LABUH KAPAL TANKER</option>
                                <option value="IDBXT.A03">AREA LABUH KAPAL BULK CARRIER</option>
                                <option value="IDBXT.A04">AREA LABUH KAPAL BOAT / TONGKANG</option>
                                <option value="IDBXT.T10.M08">TUKS PT. KARYA WIRAPUTRA BONTANG</option>
                                <option value="IDBXT.T09.MB05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I BARAT</option>
                                <option value="IDBXT.T11.J01">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA III A</option>
                                <option value="IDBXT.T11.J02">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA I B</option>
                                <option value="IDBXT.T11.J03">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA III B</option>
                                <option value="IDBXT.T11.M01">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA V</option>
                                <option value="IDBXT.T09.B01">TERSUS PT. PUPUK KALIMANTAN TIMUR PGP 1 - TURSINA TIMUR</option>
                                <option value="IDBXT.T09.B02">TERSUS PT. PUPUK KALIMANTAN TIMUR PGP 2 - TANJUNG HARAPAN</option>
                                <option value="IDBXT.T11.J04">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA II B</option>
                            </select>
                            <input class="form-control" readonly id="id-ppk" name="id" type="hidden">
                            <input class="form-control" readonly id="dermaga-kode" name="dermaga_kode" type="hidden">
                            <input class="form-control" readonly id="pkk-nomor" name="pkk_nomor" type="hidden">
                            <input class="form-control" readonly id="loa" name="loa" type="hidden">
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Tanggal Rencana</b></label>
                            <input class="form-control"  name="TanggalRencana" type="date" value="<?=$rpkro['TanggalRencana'];?>">
                        </div>
                        
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jam Rencana</b></label>
                            <input class="form-control"  name="JamRencana" type="time" value="<?=$rpkro['JamRencana'];?>">
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Gambar Tambatan</b></label>
                            <input class="form-control" id="gambar-tambatan" name="gambar_tambatan" type="file">
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Keterangan</b></label>
                            <input class="form-control"  name="keterangan" type="text"  value="<?=$rpkro['Keterangan'];?>">
                        </div>
                    </div>
                </div>


                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-1"><b>Detail RPKRO</b></h6>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Nomor RKBM</b></label>
                            <select class="form-control select2" id="NomorRkbmMuat" name="NomorRkbmMuat">
                                <option selected="selected" value=""> -- Pilih --</option>
                            </select>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Nomor PPKB</b></label>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon1">IDBXT-</span>
                                </div>
                                <input class="form-control" id="NomorPPKB" name="NomorPPKB" type="text">
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Komoditi</b></label>
                            <input class="form-control" placeholder="Komoditi" id="Komoditi" name="Komoditi" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Bongkar</b></label>
                            <div class="input-group">
                                <input class="form-control" placeholder="Bongkar" id="kegiatan-bongkar" name="kegiatan_bongkar" type="text">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Ton</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Muat</b></label>
                            <div class="input-group">
                                <input class="form-control" placeholder="Muat" id="kegiatan-muat" name="kegiatan_muat" type="text">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Ton</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Tanggal Mulai</b></label>
                            <div class="input-group">
                                <input class="form-control" required placeholder="Tanggal Mulai" id="TanggalMulaiTambat" name="TanggalMulaiTambat" type="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jam Mulai</b></label>
                            <div class="input-group">
                                <input class="form-control" required placeholder="Jam Mulai" id="JamMulaiTambat" name="JamMulaiTambat" type="time">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Tanggal Selesai</b></label>
                            <div class="input-group">
                                <input class="form-control" required placeholder="Tanggal Selesai" id="TanggalSelesaiTambat" name="TanggalSelesaiTambat" type="date">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jam Selesai</b></label>
                            <div class="input-group">
                                <input class="form-control" placeholder="Jam Selesai" id="JamSelesaiTambat" name="JamSelesaiTambat" type="time">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Kade Meter Awal</b></label>
                            <div class="input-group">
                                <input class="form-control" onBlur="kadeAkhir()" required placeholder="Kade Meter Awal" id="KadeMeterAwal" name="KadeMeterAwal" type="text" value="0">
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Kade Meter Akhir</b></label>
                            <div class="input-group">
                                <input class="form-control" required placeholder="Kade Meter Akhir" id="KadeMeterAkhir" name="KadeMeterAkhir" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="card-footer">
                    <div class="row" style="float:right;">

                    </div>
                </div>
            </div>
            </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>

<?php  $this->load->view('template/footer'); ?>\
<script>
var url_apps = "<?=base_url();?>"

$('#jenis_rpkro').on('change', function(){
    var jenis_rpkro = $(this).val();
    $("#NomorLayanan").load('<?php echo base_url('rpkro/get_ppk/');?>'+ jenis_rpkro);
});


$('#NomorLayanan').on('change', function() {
  var idSiswa = $(this).val();
  var jenis = document.getElementById("jenis_rpkro").value;
  $.ajax({
    url: url_apps + 'rpkro/get_nomor/' + $(this).val() + '/' + jenis,
    type: 'GET',
    dataType: 'json',
  })
  .done(function(data) {
    // $("#nama_barang_real").val(data.nama_barang);
    // alert(data);
    $("#NomorRpkRo").val(data.rpkro);
    $("#NomorPPKB").val(data.ppkb);
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
});
</script>