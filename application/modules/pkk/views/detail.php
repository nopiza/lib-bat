  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Detail Data PKK</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
              <li class="breadcrumb-item active">PKK</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
              <div class="card-tools">
                <a href="<?=base_url('pkk');?>" class="btn btn-warning btn-sm" ><i class="glyphicon glyphicon-refresh"></i> Back</a>
              </div>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

        <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

    
                        <div class="form-group row">
                            <label class="control-label col-md-2">Tanggal PKK</label>
                            <div class="col-md-7">
                                <input name="tipe_kendaraan" value="<?=$pkk['tgl_data'];?>" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Nomor Pkk</label>
                            <div class="col-md-7">
                                <input name="plat_nomor" value="<?=$pkk['nomor_pkk'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Nama Perusahaan</label>
                            <div class="col-md-7">
                                <input name="keterangan" value="<?=$pkk['nama_perusahaan'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Nama Kapal</label>
                            <div class="col-md-7">
                                <input name="keterangan" value="<?=$pkk['nama_kapal'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Nahkoda</label>
                            <div class="col-md-7">
                                <input name="keterangan" value="<?=$pkk['nahkoda'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">DRT</label>
                            <div class="col-md-1">
                                <input name="keterangan" value="<?=rupiah($pkk['drt']);?>"  class="form-control" type="text">
                            </div>
                            <label class="control-label col-md-1">GRT</label>
                            <div class="col-md-1">
                                <input name="keterangan" value="<?=rupiah($pkk['grt']);?>"  class="form-control" type="text">
                            </div>
                            <label class="control-label col-md-1">LOA</label>
                            <div class="col-md-1">
                                <input name="keterangan" value="<?=rupiah($pkk['loa'], 2);?>"  class="form-control" type="text">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Jenis Trayek</label>
                            <div class="col-md-7">
                                <input name="keterangan" value="<?=$pkk['jenis_trayek'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Bendera</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['bendera'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                            <label class="control-label col-md-1">Call Sign</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['call_sign'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Pelabuhan Asal</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['pelabuhan_asal'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                            <label class="control-label col-md-2">Kode Pelabuhan Asal</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['kode_pelabuhan_asal'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Pelabuhan Tujuan</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['pelabuhan_tujuan'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                            <label class="control-label col-md-2">Kode Pelabuha Tujuan</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['kode_pelabuhan_tujuan'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Dermaga</label>
                            <div class="col-md-5">
                                <input name="keterangan" value="<?=$pkk['dermaga_nama'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                            <label class="control-label col-md-1">Jenis Barang</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['jenis_barang'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Pelabuhan Muat</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['pelabuhan_muat'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                            <label class="control-label col-md-1">Kode Pelabuhan</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['kode_pelabuhan_muat'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-md-2">Pelabuhan Akhir</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['pelabuhan_tujuan_akhir'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                            <label class="control-label col-md-1">Kode Pelabuhan Akhir</label>
                            <div class="col-md-2">
                                <input name="keterangan" value="<?=$pkk['kode_tujuan_akhir_pelabuhan'];?>"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                    </div>
                </form>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>

</body>
</html>


<?php  $this->load->view('template/footer'); ?>