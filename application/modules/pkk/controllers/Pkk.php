<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkk extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'pkk');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pkk_model','pkk');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('view',$user_data);

	}

	public function detail($id)
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$user_data['pkk'] = $this->db->query("SELECT * FROM pkk WHERE id='$id'")->row_array();
      	$this->load->view('template/header',$user_data);
      	$this->load->view('detail',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->pkk->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-info" href="'.base_url('pkk/detail/'.$post->id).'"> View Detail</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nomor_pkk;
			$row[] = $post->nama_kapal;
			$row[] = $post->nama_perusahaan;
			if($post->stt_proses == '0'){
				$row[] = '<span class="badge badge-pill badge-secondary">Baru</span>';
			}else if($post->stt_proses == '1'){
				$row[] = '<span class="badge badge-pill badge-danger">dilihat</span>';
			}else if($post->stt_proses == '2'){
				$row[] = '<span class="badge badge-pill badge-warning">RPKRO</span>';
			}else if($post->stt_proses == '3'){
				$row[] = '<span class="badge badge-pill badge-info">SPK Pandu Masuk</span>';
			}else if($post->stt_proses == '4'){
				$row[] = '<span class="badge badge-pill badge-info">SPK Pandu Pindah</span>';
			}else if($post->stt_proses == '5'){
				$row[] = '<span class="badge badge-pill badge-info">SPK Pandu Keluar</span>';
			}else if($post->stt_proses == '6'){
				$row[] = '<span class="badge badge-pill badge-primary">Realisasi</span>';
			}else if($post->stt_proses == '7'){
				$row[] = '<span class="badge badge-pill badge-success">Invoice</span>';
			}else if($post->stt_proses == '8'){
				$row[] = '<span class="badge badge-pill badge-success">Invoice</span>';
			}
			
			//add html for action
			$row[] = $link_edit;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->pkk->count_all(),
						"recordsFiltered" => $this->pkk->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->pkk->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		$data = array(
			'tipe_kendaraan' 		=> $this->input->post('tipe_kendaraan'),
			'plat_nomor' 			=> $this->input->post('plat_nomor'),
			'keterangan' 			=> $this->input->post('keterangan')
		);
		$this->pkk->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
			'tipe_kendaraan' 		=> $this->input->post('tipe_kendaraan'),
			'plat_nomor' 			=> $this->input->post('plat_nomor'),
			'keterangan' 			=> $this->input->post('keterangan')
		);

		$this->pkk->update(array('id_pkk' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->db->delete('pkk',array('id_pkk'=>$id));
		echo json_encode(array("status" => TRUE));
	}

}
