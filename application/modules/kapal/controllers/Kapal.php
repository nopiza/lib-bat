<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kapal extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'kapal');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kapal_pandu_model','kapal_pandu');
		$this->load->model('Kapal_tunda_model','kapal_tunda');
		check_login();
	}

	public function index()
	{

	}

	public function pandu()
	{
		$user_data['data_ref'] = $this->data_ref;
		$user_data['title'] = 'Menu';
		$this->load->view('template/header');
		$this->load->view('kapal_pandu',$user_data);
	}

	public function tunda()
	{
		$user_data['data_ref'] = $this->data_ref;
		$user_data['title'] = 'Menu';
		$this->load->view('template/header');
		$this->load->view('kapal_tunda',$user_data);
	}

	public function ajax_list()
	{

		$list = $this->kapal_pandu->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_kapal_pandu."'".')"> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_kapal_pandu."'".')"> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_kapal_pandu;
         	$row[] = $post->keterangan;

			//add html for action
			$row[] = $link_edit.$link_hapus;
			$data[] = $row;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->kapal_pandu->count_all(),
					"recordsFiltered" => $this->kapal_pandu->count_filtered(),
					"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->kapal_pandu->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		aktifitas('Login','Melakukan Penambahan kapal Pandu');
		$data = array(
				'nama_kapal_pandu' 		=> $this->input->post('kapal_pandu'),
				'keterangan' 			=> $this->input->post('keterangan')
		);
		$this->kapal_pandu->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		aktifitas('Login','Melakukan Edit kapal Pandu');
		$data = array(
			'nama_kapal_pandu' 		=> $this->input->post('kapal_pandu'),
			'keterangan' 			=> $this->input->post('keterangan')
		);
		
		$this->kapal_pandu->update(array('id_kapal_pandu' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{
		aktifitas('Login','Melakukan Hapus kapal Pandu');
		$this->db->delete('kapal_pandu', ['id_kapal_pandu' => $id]);
		echo json_encode(array("status" => TRUE));
	}

	

	// Kapal TUNDA ==============================================================================================================>>>>

	public function ajax_list_tunda()
	{

		$list = $this->kapal_tunda->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->id_kapal_tunda."'".')"> Edit</a>';
			$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->id_kapal_tunda."'".')"> Delete</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nama_kapal_tunda;
         	$row[] = $post->keterangan;

			//add html for action
			$row[] = $link_edit.$link_hapus;
			$data[] = $row;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->kapal_tunda->count_all(),
					"recordsFiltered" => $this->kapal_tunda->count_filtered(),
					"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_edit_tunda($id)
	{
		$data = $this->kapal_tunda->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add_tunda()
	{
		aktifitas('Login','Melakukan Penambahan kapal Tunda');
		$data = array(
				'nama_kapal_tunda' 		=> $this->input->post('kapal_tunda'),
				'keterangan' 			=> $this->input->post('keterangan')
		);
		$this->kapal_tunda->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update_tunda()
	{
		aktifitas('Login','Melakukan Edit kapal Tunda');
		$data = array(
			'nama_kapal_tunda' 		=> $this->input->post('kapal_tunda'),
			'keterangan' 			=> $this->input->post('keterangan')
		);
		
		$this->kapal_tunda->update(array('id_kapal_tunda' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete_tunda($id)
	{
		aktifitas('Login','Melakukan Hapus kapal Tunda');
		$this->db->delete('kapal_tunda', ['id_kapal_tunda' => $id]);
		echo json_encode(array("status" => TRUE));
	}



}
