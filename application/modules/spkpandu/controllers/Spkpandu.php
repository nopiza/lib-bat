<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spkpandu extends CI_Controller {

   var $data_ref = array('uri_controllers' => 'spkpandu');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Spkpandu_model','spkpandu');
		// $this->load->model('Group/Group_model','group');

		check_login();
	}

	public function index()
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$this->load->view('template/header',$user_data);
      	$this->load->view('view',$user_data);
	}

	public function create()
	{
      	$user_data['data_ref'] = $this->data_ref;
		$user_data['kapalPandu'] = $this->db->get('kapal_pandu')->result();
		$user_data['kapalTunda'] = $this->db->get('kapal_tunda')->result();
      	$this->load->view('template/header',$user_data);
      	$this->load->view('create',$user_data);
	}

	public function detail($id)
	{
      	$user_data['data_ref'] = $this->data_ref;
      	$user_data['pkk'] = $this->db->query("SELECT * FROM pkk WHERE id='$id'")->row_array();
      	$this->load->view('template/header',$user_data);
      	$this->load->view('detail',$user_data);

	}

	public function ajax_list()
	{

		$list = $this->spkpandu->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $post) {
			$link_edit = '<a class="btn btn-xs btn-info" href="'.base_url('spkpandu/detail/'.$post->id_simpan_spk).'"> View Detail</a>';
			$link_cetak_spk = '<a class="btn btn-xs btn-success" href="'.base_url('spkpandu/cetak/'.$post->id_simpan_spk).'" target="_blank"> Cetak SPK</a>';
			$menunggu = '<a class="btn btn-xs btn-warning" href="#"> Menunggu SPOG</a>';
			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->nomor_pkk.'<br><span class="badge badge-info">'.$post->nama_kapal.'</span>';
			$row[] = $post->NomorSPKPandu.'<br>'.$post->NomorPPK;
			$row[] = $post->TanggalPandu.''.$post->JamPandu;
			$row[] = $post->NamaPetugasPandu;
			$row[] = '<b>Awal : </b>'.$post->LokasiAwal.'<br><b>Akhir : </b>'.$post->LokasiAkhir;
			$jeson = json_encode($post->json_spk);
			$row[] = $post->Kegiatan;
			// cek di data SPOG
			$cek = $this->db->query("SELECT * FROM spog WHERE nomor_spk='$post->NomorSPKPandu'")->num_rows();
			if($cek){
				$row[] = $link_cetak_spk;
			}else{
				$row[] = $menunggu;
			}
			//add html for action
			$row[] = $link_edit;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->spkpandu->count_all(),
						"recordsFiltered" => $this->spkpandu->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->spkpandu->get_by_id($id);
		echo json_encode($data);
	}

	public function save()
	{
	
		

		for($i=1; $i < 2; $i++){
			if($this->input->post('kapal_pandu['.$i.']') != NULL){
				$param['NamaKapalPandu'.$i]  = $this->input->post('kapal_pandu['.$i.']');
				$param['NamaKapalPandu'.$i];
			}else{
				$a = '';
			}
		}

		for($i=1; $i < 5; $i++){
			if($this->input->post('kapal_tunda['.$i.']') != NULL){
				$param['NamaKapalTunda'.$i]  = $this->input->post('kapal_tunda['.$i.']');
				$param['NamaKapalTunda'.$i];
			}else{
				$a = '';
			}
		}

		

		$res = '';
		try {	
			$soapClient = new SoapClient('https://inaportdev.dephub.go.id/v1/inaportnet-service-v3?wsdl');
			$object = new stdClass();
			$object->user = $this->input->post('username');
			$object->password = $this->input->post('password');
			// Data
			$object->NomorPKK			 	= $this->input->post('NomorLayanan');
			$object->NomorPPK 				= $this->input->post('nomor_ppk');
			$object->NomorSPKPandu 			= $this->input->post('no_spk');
			$object->NamaPetugasPandu 		= $this->input->post('nama_petugas_pandu');
			$object->TanggalPandu 			= $this->input->post('pandu_tanggal');
			$object->JamPandu 				= $this->input->post('pandu_jam');
			$object->NamaKapalPandu1 		= $this->input->post('kapal_pandu_1');
			$object->NamaKapalPandu2 		= $this->input->post('NamaKapalPandu2');
			$object->NamaKapalTunda1 		= $this->input->post('kapal_tunda_1');
			$object->NamaKapalTunda2 		= $this->input->post('NamaKapalTunda2');
			$object->NamaKapalTunda3 		= $this->input->post('NamaKapalTunda3');
			$object->NamaKapalTunda4 		= $this->input->post('NamaKapalTunda4');
			$object->JenisPandu 			= $this->input->post('jenis_pandu');
			$object->LokasiAwal 			= $this->input->post('kode_dermaga_dari');
			$object->LokasiAkhir 			= $this->input->post('kode_dermaga_ke');
			$object->Kegiatan 				= $this->input->post('tujuan');
			$object->WaktuGerak 			= $this->input->post('waktu_gerak');

			$results = $soapClient->SetSpkPandu($object);
			$res = json_encode($results);

			$param = [
				'NomorPKK'			=> $this->input->post('NomorLayanan'),
				'NomorPPK'			=> $this->input->post('nomor_ppk'),
				'NomorSPKPandu'		=> $this->input->post('no_spk'),
				'NamaPetugasPandu'	=> $this->input->post('nama_petugas_pandu'),
				'TanggalPandu'		=> $this->input->post('pandu_tanggal'),
				'JamPandu'			=> $this->input->post('pandu_jam'),
				'JenisPandu'		=> $this->input->post('jenis_pandu'),
				'LokasiAwal'		=> $this->input->post('kode_dermaga_dari'),
				'LokasiAkhir'		=> $this->input->post('kode_dermaga_ke'),
				'Kegiatan'			=> $this->input->post('tujuan'),
				'WaktuGerak'		=> $this->input->post('waktu_gerak'),
				'NomorLayanan' 		=> $this->input->post('NomorLayanan'),
				'json_spk' 			=> $res
			];

			$this->db->insert('z_simpan_spkpandu', $param);

			// Nomor RPKRO
			$noSPKPandu = explode('-', $this->input->post('no_spk'));
			$paramAPKPandu = [
				'nomor_surat'		=> str_replace('IDBXT-', '', $this->input->post('no_spk')),
				'jenis_kegiatan'	=> 'SPK PANDU '.$noSPKPandu[3],
				'bln_thn'			=> $noSPKPandu[4],
				'id_kegiatan'		=> '1',
				'keterangan'		=> ''
			];
			$this->db->insert('penomoran', $paramAPKPandu);
			// echo json_encode($results);

			// die(var_dump($results->entryRpkroResult->return));
			// if($results->entryRpkroResult->return->statusCode!='01'){
			// 	die($results->entryRpkroResult->return->statusMessage);
			// }
			// berhasil
			

			// var_dump($results->authenticateResult);
		} catch(Exception $e) {
			die($e->getMessage());
		}
		redirect('spkpandu');
	}


	function get_ppk($noPKK){
		// cari no RPKRO di tabel _rpkro
		$rpkro = $this->db->query("SELECT * FROM z_simpan_rpkro WHERE NomorLayanan = '$noPKK' ORDER BY id_simpan_rpkro DESC LIMIT 1")->row_array();
		$no = $rpkro['NomorRpkRo'];
		$ppk = $this->db->query("SELECT * FROM ppk WHERE nomor_rpkro = '$no' ORDER BY nomor_ppk DESC limit 1")->row_array();
		// Cek posisi Kapal | Masuk | Pindah | Keluar
		$ppk['noSPKPandu'] = penomoran('SPK PANDU IN', '');
		return $this->output->set_content_type('application/json')->set_output(json_encode($ppk));      
	}

	function get_no_pandu($jenis){
		// cari no RPKRO di tabel _rpkro
		if($jenis == 'Masuk'){
			$item['nomor'] = penomoran('SPK PANDU IN');
		}else if($jenis == 'Pindah'){
			$item['nomor'] = penomoran('SPK PANDU KP');
		}else if($jenis == 'Keluar'){
			$item['nomor'] = penomoran('SPK PANDU OUT');
		}
		
		return $this->output->set_content_type('application/json')->set_output(json_encode($item));      
	}

	public function cetak($id)
	{
      	$cetak = $this->db->query("SELECT * FROM z_simpan_spkpandu z 
		LEFT JOIN pkk k ON z.NomorPKK = k.nomor_pkk 
		WHERE z.id_simpan_spk = '$id'")->row_array();


$config=array('orientation'=>'P','size' => 'A4');
        $this->load->library('MyPDF',$config);
        $this->mypdf->AddPage();
        $this->mypdf->AddFont('cambria','','cambria.php');

        $this->mypdf->Cell(137,8,'',0,0,'C');
        $this->mypdf->Cell(137,8,'',0,1,'C');

		$this->mypdf->ln(2);
        $this->mypdf->SetFont('Times','B',14);
        $this->mypdf->Cell(5,5,'',0,0,'C');
        $this->mypdf->Cell(170,5,'Penerbitan SPK (Surat Perintah Kerja)',0,1,'C');


		$this->mypdf->ln(12);
		$this->mypdf->SetFont('Times','B',12);
		$this->mypdf->Cell(5,6,'',0,0,'C');
        $this->mypdf->Cell(170,5,'I. DATA KAPAL',0,1,'L');

		$this->mypdf->SetFont('Times','',12);
		$this->mypdf->ln(2);
		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'1. Tanda Pendaftaran Kapal',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['tanda_pendaftaran_kapal'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'2. Nama Kapal',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['nama_kapal'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'3. Call Sign',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['call_sign'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'4. Nama Nahkoda',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['nahkoda'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'5. IMO Number',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['imo_number'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'6. Bendera',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['bendera'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'7. Jenis Kapal',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['jenis_kapal'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'8. Tahun Pembuatan',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['tahun_pembuatan'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'9. GT',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['grt'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'10. Panjang Kapal / LOA',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['loa'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'11. Lebar Kapal',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['lebar_kapal'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'12. Draft Depan',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['draft_depan'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'13. Draft Belakang',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['draft_belakang'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'14. Pelabuhan Asal',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['pelabuhan_asal'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'15. Pelabuhan Tujuan',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['pelabuhan_tujuan'],0,1,'L');

// ----------------------------------->
		$this->mypdf->ln(12);
		$this->mypdf->SetFont('Times','B',12);
		$this->mypdf->Cell(5,6,'',0,0,'C');
        $this->mypdf->Cell(170,5,'I. DATA SPK PANDU',0,1,'L');

		$this->mypdf->SetFont('Times','',12);
		$this->mypdf->ln(2);
		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'1. Nomor SPK Pandu',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NomorSPKPandu'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'2. Nomor PKK',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NomorPKK'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'3. Nomor PPK',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NomorPPK'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'4. Tanggal Pandu',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['TanggalPandu'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'5. Jam Pandu',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['JamPandu'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'6. Nama Petugas Pandu',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NamaPetugasPandu'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'7. Nama Kapal Pandu',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NamaKapalPandu1'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'8. Nama Kapal Pandu 2',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NamaKapalPandu2'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'9. Nama Kapal Tunda',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NamaKapalTunda1'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'10. Nama Kapal Tunda 2',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NamaKapalTunda2'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'11. Nama Kapal Tunda 3',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NamaKapalTunda3'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'12. Nama Kapal Tunda 4',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['NamaKapalTunda4'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'13. GT',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['pelabuhan_tujuan'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'14. Jenis Pandu',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['JenisPandu'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'15. Lokasi Awal',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['LokasiAwal'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'16. Lokasi Akhir',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['LokasiAkhir'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'17. Tujuan',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['Kegiatan'],0,1,'L');

		$this->mypdf->Cell(10,6,'',0,0,'C');
        $this->mypdf->Cell(70,6,'18. WAktu Gerak',0,0,'L');
        $this->mypdf->Cell(5,6,':',0,0,'L');
        $this->mypdf->Cell(170,6,$cetak['WaktuGerak'],0,1,'L');

		




		
		
        $this->mypdf->Output('I','INV-'); 



	}

	public function ajax_delete($id)
	{
		$this->db->delete('ppk',array('id_pkk'=>$id));
		echo json_encode(array("status" => TRUE));
	}

}
