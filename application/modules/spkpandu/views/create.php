  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Surat Perintah Kerja</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Create SPK Pandu</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Form SPK Pandu</h3>
              <!-- /.card-tools -->
        </div>

        <!-- /.card-header -->
        <div class="card-body">

        <form action="<?=base_url('spkpandu/save');?>" id="form" class="form-horizontal" method="POST">

            <div class="form-group row">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-md-5">
                <input name="username" type="text" class="form-control" id="username" autocomplete="OFF" value="lintas-bontang" readonly>
                <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-md-5">
                <input name="password" type="password" class="form-control" id="password" value="23Okto1991" readonly>
                <span class="help-block"></span>
                </div>
            </div>

            <hr>

            <div>
                        <h6 class="main-content-label mb-1"><b>Permohonan SPK Pandu</b></h6>
                    </div>
                    <hr>

                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Nomor PKK</b></label>
                            <select class="form-control" id="NomorLayanan" required name="NomorLayanan">
                              <option value="">-- pilih --</option>
                              <?php 
                              $layanan = $this->db->query("SELECT nomor_pkk, nama_kapal FROM pkk")->result();
                              foreach($layanan as $ly){
                                echo '<option value="'.$ly->nomor_pkk.'">'.$ly->nama_kapal.' # '.$ly->nomor_pkk.'</option>';
                              }
                              ?>
                            </select>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Nomor PPK</b></label>
                            <input class="form-control" required id="ppk-nomor" name="nomor_ppk" type="text" readonly>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>No SPK Pandu</b></label>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon1">IDBXT-</span>
                                </div>
                                <input class="form-control" required id="nomor-spk" name="no_spk" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Petugas Pandu</b></label>
                            <input class="form-control" required id="petugas-pandu" name="nama_petugas_pandu" type="text">
                            <input class="form-control" readonly id="id-spk" name="id" type="hidden">
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Tanggal Pandu</b></label>
                            <input class="form-control" required name="pandu_tanggal" type="date">
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jam Pandu</b></label>
                            <input class="form-control" required name="pandu_jam" type="time">
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Kapal Pandu - Max 2 Kapal</b></label>
                            <select class="js-example-basic-multiple w-100" multiple="multiple" name="kapal_pandu[]" id="kapal_pandu">
                            <?php 
                                foreach($kapalPandu as $pk){
                                    echo '<option value="'.$pk->nama_kapal_pandu.'" data-select2-id="125">'.$pk->nama_kapal_pandu.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label"><b>Kapal Tunda - Max 4 Kapal</b></label>
                            <select class="js-example-basic-multiple_2 form-control" multiple="multiple" id="kapal-tunda[]" name="kapal_tunda[]">
                                <?php 
                                foreach($kapalTunda as $kt){
                                    echo '<option value="'.$kt->nama_kapal_tunda.'" data-select2-id="125">'.$kt->nama_kapal_tunda.'</option>';
                                }
                                ?>
                            </select>

                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Lokasi Awal</b></label>
                            <select class="form-control select2 required" id="kode-dermaga-dari" required name="kode_dermaga_dari"><option value="LAUT">LAUT</option><option value="IDBXT.A04">AREA LABUH KAPAL BOAT / TONGKANG</option><option value="IDBXT.A03">AREA LABUH KAPAL BULK CARRIER</option><option value="IDBXT.A02">AREA LABUH KAPAL TANKER</option><option value="IDBXT.A01">AREA LABUH SHIP TO SHIP</option><option value="IDBXT.T06.J06">PELABUHAN UMUM LOK TUAN DERMAGA I</option><option value="IDBXT.T06.J07">PELABUHAN UMUM LOK TUAN DERMAGA II</option><option value="IDBXT.T06.J08">PELABUHAN UMUM LOK TUAN DERMAGA III</option><option value="IDBXT.T06.J05">PELABUHAN UMUM TANJUNG LAUT</option><option value="IDBXT.T07.M04">TERSUS PT. BLACK BEAR RESOURCES INDONESIA</option><option value="IDBXT.T11.J02">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA I B</option><option value="IDBXT.T11.J04">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA II B</option><option value="IDBXT.T11.J01">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA III A</option><option value="IDBXT.T11.J03">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA III B</option><option value="IDBXT.T11.M01">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA V</option><option value="IDBXT.T01.J01">TERSUS PT. INDOMINCO MANDIRI DERMAGA I</option><option value="IDBXT.T01.J02">TERSUS PT. INDOMINCO MANDIRI DERMAGA II</option><option value="IDBXT.T01.J03">TERSUS PT. INDOMINCO MANDIRI DERMAGA III</option><option value="IDBXT.T01.M01">TERSUS PT. INDOMINCO MANDIRI DERMAGA IV</option><option value="IDBXT.T01.S01">TERSUS PT. INDOMINCO MANDIRI FTS</option><option value="IDBXT.T08.O01">TERSUS PT. KALTIM METHANOL INDUSTRI</option><option value="IDBXT.T09.MB05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I BARAT</option><option value="IDBXT.T09.MS05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I SELATAN</option><option value="IDBXT.T09.MU05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I UTARA</option><option value="IDBXT.T09.M06">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA II</option><option value="IDBXT.T09.O02">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA III</option><option value="IDBXT.T09.M07">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA IV</option><option value="IDBXT.T09.O03">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VI</option><option value="IDBXT.T09.D01">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VII</option><option value="IDBXT.T09.O04">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VIII</option><option value="IDBXT.T09.B01">TERSUS PT. PUPUK KALIMANTAN TIMUR PGP 1 - TURSINA TIMUR</option><option value="IDBXT.T09.B02">TERSUS PT. PUPUK KALIMANTAN TIMUR PGP 2 - TANJUNG HARAPAN</option><option value="IDBXT.T02.J04">TUKS PT. GRAHA POWER KALTIM</option><option value="IDBXT.T04.M03">TUKS PT. HARLIS TATA TAHTA</option><option value="IDBXT.T10.M08">TUKS PT. KARYA WIRAPUTRA BONTANG</option><option value="IDBXT.T03.M02">TUKS PT. PERTAMINA DERMAGA CARGO</option><option value="IDBXT.T03.D01">TUKS PT. PERTAMINA DERMAGA DOCKING</option><option value="IDBXT.T03.J05">TUKS PT. PERTAMINA DERMAGA I</option><option value="IDBXT.T03.J06">TUKS PT. PERTAMINA DERMAGA II</option><option value="IDBXT.T03.J07">TUKS PT. PERTAMINA DERMAGA III</option></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Lokasi Akhir</b></label>
                            <select class="form-control select2 required" id="kode-dermaga-ke" required name="kode_dermaga_ke"><option value="LAUT">LAUT</option><option value="IDBXT.A04">AREA LABUH KAPAL BOAT / TONGKANG</option><option value="IDBXT.A03">AREA LABUH KAPAL BULK CARRIER</option><option value="IDBXT.A02">AREA LABUH KAPAL TANKER</option><option value="IDBXT.A01">AREA LABUH SHIP TO SHIP</option><option value="IDBXT.T06.J06">PELABUHAN UMUM LOK TUAN DERMAGA I</option><option value="IDBXT.T06.J07">PELABUHAN UMUM LOK TUAN DERMAGA II</option><option value="IDBXT.T06.J08">PELABUHAN UMUM LOK TUAN DERMAGA III</option><option value="IDBXT.T06.J05">PELABUHAN UMUM TANJUNG LAUT</option><option value="IDBXT.T07.M04">TERSUS PT. BLACK BEAR RESOURCES INDONESIA</option><option value="IDBXT.T11.J02">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA I B</option><option value="IDBXT.T11.J04">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA II B</option><option value="IDBXT.T11.J01">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA III A</option><option value="IDBXT.T11.J03">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA III B</option><option value="IDBXT.T11.M01">TERSUS PT. ENERGI UNGGUL PERSADA DERMAGA V</option><option value="IDBXT.T01.J01">TERSUS PT. INDOMINCO MANDIRI DERMAGA I</option><option value="IDBXT.T01.J02">TERSUS PT. INDOMINCO MANDIRI DERMAGA II</option><option value="IDBXT.T01.J03">TERSUS PT. INDOMINCO MANDIRI DERMAGA III</option><option value="IDBXT.T01.M01">TERSUS PT. INDOMINCO MANDIRI DERMAGA IV</option><option value="IDBXT.T01.S01">TERSUS PT. INDOMINCO MANDIRI FTS</option><option value="IDBXT.T08.O01">TERSUS PT. KALTIM METHANOL INDUSTRI</option><option value="IDBXT.T09.MB05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I BARAT</option><option value="IDBXT.T09.MS05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I SELATAN</option><option value="IDBXT.T09.MU05">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA I UTARA</option><option value="IDBXT.T09.M06">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA II</option><option value="IDBXT.T09.O02">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA III</option><option value="IDBXT.T09.M07">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA IV</option><option value="IDBXT.T09.O03">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VI</option><option value="IDBXT.T09.D01">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VII</option><option value="IDBXT.T09.O04">TERSUS PT. PUPUK KALIMANTAN TIMUR DERMAGA VIII</option><option value="IDBXT.T09.B01">TERSUS PT. PUPUK KALIMANTAN TIMUR PGP 1 - TURSINA TIMUR</option><option value="IDBXT.T09.B02">TERSUS PT. PUPUK KALIMANTAN TIMUR PGP 2 - TANJUNG HARAPAN</option><option value="IDBXT.T02.J04">TUKS PT. GRAHA POWER KALTIM</option><option value="IDBXT.T04.M03">TUKS PT. HARLIS TATA TAHTA</option><option value="IDBXT.T10.M08">TUKS PT. KARYA WIRAPUTRA BONTANG</option><option value="IDBXT.T03.M02">TUKS PT. PERTAMINA DERMAGA CARGO</option><option value="IDBXT.T03.D01">TUKS PT. PERTAMINA DERMAGA DOCKING</option><option value="IDBXT.T03.J05">TUKS PT. PERTAMINA DERMAGA I</option><option value="IDBXT.T03.J06">TUKS PT. PERTAMINA DERMAGA II</option><option value="IDBXT.T03.J07">TUKS PT. PERTAMINA DERMAGA III</option></select>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Jenis Pandu</b></label>
                            <select class="form-control select2" id="jenis-pandu" required name="jenis_pandu"><option value="SUNGAI">Sungai</option><option value="LAUT">Laut</option><option value="BANDAR">Bandar</option><option value="LUAR BIASA">Luar Biasa</option></select>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Keperluan / Kegiatan</b></label>
                            <select class="form-control select2" id="tujuan" required name="tujuan">
                                <option value="">-- pilih -- </option>
                                <option value="Masuk">Masuk</option>
                                <option value="Pindah">Pindah</option>
                                <option value="Keluar">Keluar</option>
                                <option value="Sea Trial">Sea Trial</option>
                                <option value="Labuh">Labuh</option>
                                <option value="Transhipment">Transhipment</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4 col-sm-12">
                            <label class="control-label has-star"><b>Waktu Gerak</b></label>
                            <input class="form-control" id="waktu-gerak" required name="waktu_gerak" type="datetime-local">
                        </div>
                    </div>

            <div class="form-group row">
                <label class="col-sm-2 control-label"></label>
                <div class="col-md-5">
                <button type="submit" id="submit" class="btn btn-primary btn-sm">Kirim Data</button>
                <!-- <button type="button" id="btnSave" onclick="save()" class="btn btn-primary btn-sm">Kirim Data</button> -->
                </div>
            </div>

            
        </form>

        <br>
        <hr>

        <pre id="beautified"></pre>

          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
</div>


</body>
</html>


<?php  $this->load->view('template/footer'); ?>

<script src="<?php echo base_url('assets/plugins/select2/select2.min.js')?>"></script>
<!-- <script src="<?php echo base_url('js/select2.js')?>"></script> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/select2/select2.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/select2/select2-bootstrap.css') ?>">


<script>
var url_apps = "<?=base_url();?>"
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
    $('.js-example-basic-multiple_2').select2();
});

function save()
   {
       $('#btnSave').text('Menyimpan...'); //change button text
       $('#btnSave').attr('disabled',true); //set button disable 
       var url = "<?php echo site_url('spkpandu/save')?>";

       // ajax adding data to database
       $.ajax({
           url : url,
           type: "POST",
           data: $('#form').serialize(),
           dataType: "JSON",
           success: function(data)
           {
    

                // Lobibox.notify('success', {
                //     size: 'mini',
                //     msg: 'Data berhasil Disimpan'
                // });
                document.getElementById("beautified").innerHTML = JSON.stringify(data, undefined, 2);
                $('#btnSave').text('Simpan'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
    
    
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
               $('#btnSave').text('Simpan'); //change button text
               $('#btnSave').attr('disabled',false); //set button enable 
    
           }
       });
   }


    // var data = {"authenticateResult":{"statusCode":"01","statusMessage":"Sukses","userInfo":{"id":47741,"username":"Lintas-bontang","email":"lintasinternasionalberkarya@gmail.com","status":"10","kode_pelabuhan":"IDBXT","fullname":"Edo nusa herlangga","auth_name":"BUP","jabatan":"staff","bidang":"","login_attempt":"0"},"organisasi":{"kode_kantor":"KNT.2208.000001","kode_perusahaan":"PRS.2208.000001","nama":"PT.lintas internasional berkarya","npwp":"\/v1\/doman\/download\/get?namafile=42.347.324.8-721.000","alamat":"JL.AHMAD YANI,PERUM HALAL SQUARE BLOCK G NO 7 ,BONTANG UTARA KOTA BONTANG","siupal":"1223000502605","tgl_siupal":"2021-05-25","akta":"04","golongan_usaha":"BUP","kode_tipe_perusahaan":"BUP","firma":"PT. ","domisili":"64.74","telp":"08115954455","fax":"","penanggung_jawab":"MARGO SUCIPTO","ktp":"20220801032813.KTP_penaggung_jawab.pdf","kode_wilayah":"64.74","tgl_registrasi":"2022-08-01 03:28:13","tgl_expired":"2023-08-01","no_pmku":"PMKU.IDBXT.0822.000001","tgl_pmku":"2022-08-01 03:30:43","file_npwp":"\/v1\/doman\/download\/get?namafile=20220801032813.NPWP_PERUSAHAAN.pdf","file_siup":"\/v1\/doman\/download\/get?namafile=20220801032813.SURAT_IJIN_USAHA%280%29.pdf","file_organisasi":"\/v1\/doman\/download\/get?namafile=20220801032813.STRUKTUR_ORGANISASI_PT._LIB_NEW.pdf","file_domisili":"\/v1\/doman\/download\/get?namafile=20220801032813.domisili%280%29.pdf","file_siupkumham":"\/v1\/doman\/download\/get?namafile=20220801032813.siup_kum_ham_compressed.pdf"}}}
    // document.getElementById("beautified").innerHTML = JSON.stringify(data, undefined, 2);

$('#NomorLayanan').on('change', function() {
        $.ajax({
            url: url_apps + 'spkpandu/get_ppk/' + $(this).val(),
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            // $("#nama_barang_real").val(data.nama_barang);
            // alert(data.nomor_ppk);
            $("#ppk-nomor").val(data.nomor_ppk);
            // $("#nomor-spk").val(data.noSPKPandu);
            // $("#NomorPPKB").val(data.ppkb);
            
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        $("#NomorRkbmMuat").load('<?php echo base_url('rpkro/get_rkbm/');?>'+ $(this).val());
    });


    $('#tujuan').on('change', function() {
        $.ajax({
            url: url_apps + 'spkpandu/get_no_pandu/' + $(this).val(),
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            // $("#nama_barang_real").val(data.nama_barang);
            // alert(data.nomor_ppk);
            $("#nomor-spk").val(data.nomor);
            // $("#NomorPPKB").val(data.ppkb);
            
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        $("#NomorRkbmMuat").load('<?php echo base_url('rpkro/get_rkbm/');?>'+ $(this).val());
    });
</script>