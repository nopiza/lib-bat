<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('customade');
		$this->load->library(array('form_validation'));	
		check_login();

	}
	
	public function index()
	{
		$data=array();
		$csrf = array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
		    );
		$data=array('csrf'=>$csrf);	


		$this->load->view('template/header',$data);
		$this->load->view('dashboard',$data);
	}

	public function cekpkk()
	{
		$query = "SELECT * FROM pkk WHERE stt_proses = '0'";
		$cek = $this->db->query($query)->num_rows();
		
		if($cek){
			$data = $this->db->query($query)->row_array();
			//update data jadi 1
			$id = $data['id'];
			$this->db->query("UPDATE pkk SET stt_proses = '1' WHERE id = '$id'");
			$data['ada'] = 1;
			echo json_encode($data);
		}else{
			$data['ada'] = 0;
			echo json_encode($data);
		}
		
		
	}



	

}