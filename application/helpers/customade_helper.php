<?php

function tgl_now() {
    date_default_timezone_set('Asia/Makassar');
    return date('Y-m-d');
}

function time_now() {
    date_default_timezone_set('Asia/Makassar');
    return date('H:i:s');
}

function tglTime_now() {
    date_default_timezone_set('Asia/Makassar');
    return date('Y-m-d H:i:s');
}

function aktifitas($aktifitas, $keterangan='')
{
    $CI = &get_instance();
    date_default_timezone_set('Asia/Makassar');
    $sekarang = date('Y-m-d H:i:s');
    $param = [
        'tanggal' => $sekarang, 
        'aktifitas' => $aktifitas, 
        'keterangan' => $keterangan, 
        'id_user' => $CI->encryption->decrypt($CI->session->userdata('id')), 
        'surname' => $CI->encryption->decrypt($CI->session->userdata('surname')), 
        'username' => $CI->encryption->decrypt($CI->session->userdata('username')), 
    ];
    $CI->db->insert('aktifitas', $param);
    return('true');
}


function rupiah($nilai, $pecahan = 0) {
    return number_format($nilai, $pecahan, ',', '.');
}



function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu ", "dua ", "tiga ", "empat ", "lima ", "enam ", "tujuh ", "delapan ", "sembilan ", "sepuluh ", "sebelas ");
    $temp = "";
    if ($nilai < 12) {
        $temp = "". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = penyebut($nilai - 10). "belas ";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai/10)."puluh ". penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = "seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai/100) . "ratus " . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = "seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai/1000) . "ribu " . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai/1000000) . "juta " . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai/1000000000) . "milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai/1000000000000) . "trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
}

function check_login()
{
    $CI = &get_instance();
    if ($CI->session->userdata('username') == '') {
        redirect('');
    }
}

function getUser()
{
    $CI = &get_instance();
    $idUser = $CI->encryption->decrypt($CI->session->userdata('id'));
    $dataUser = $CI->db->query("
            SELECT * FROM users a 
            LEFT JOIN registrasi b ON a.id_join=b.id_reg 
            LEFT JOIN teknisi c ON b.id_reg=c.id_reg WHERE a.id='$idUser'
            ")->row_array();

    return $dataUser;
}

function infoUser($kolom = "")
{
    $CI = &get_instance();
    $idUser = $CI->encryption->decrypt($CI->session->userdata('id'));
    $dataUser = $CI->db->query("
            SELECT $kolom as dataKolom FROM users a 
            LEFT JOIN registrasi b ON a.id_join=b.id_reg 
            LEFT JOIN teknisi c ON b.id_reg=c.id_reg WHERE a.id='$idUser'
            ")->row_array();

    return $dataUser['dataKolom'];
}


function selamat(){
    date_default_timezone_set("Asia/makassar");
    $jam = date("H:i:s");
    if($jam <='05:00:00'){
        echo "Selamat dini hari";
    }elseif($jam >='05:00:01' AND $jam <='11:00:00'){
        echo "Selamat Pagi";
    }elseif($jam >='11:00:01' AND $jam <='15:00:00'){
        echo "Selamat Siang";
    }elseif($jam >='15:00:01' AND $jam <='18:00:00'){
        echo "Selamat Sore";
    }elseif($jam >='18:00:01'){
        echo "Selamat Malam";
    }else{
        
    }
}



function tgl_indo($tgl){
    return substr($tgl, 8, 2).' '.getbln(substr($tgl, 5,2)).' '.substr($tgl, 0, 4);
}



function namaHari($tanggal){
    $day=date("D", strtotime ($tanggal)); 
        if($day=='Mon'){
            $day='Senin';
        }else if($day=='Tue'){
            $day='Selasa';
        }else if($day=='Wed'){
            $day='Rabu';
        }else if($day=='Thu'){
            $day='Kamis';
        }else if($day=='Fri'){
            $day='Jumat';
        }else if($day=='Sat'){
            $day='Sabtu';
        }else if($day=='Sun'){
            $day='Minggu';
        }
    return $day;
}

function namaHariKecil($tanggal){
    $day=date("D", strtotime ($tanggal)); 
        if($day=='Mon'){
            $day='senin';
        }else if($day=='Tue'){
            $day='selasa';
        }else if($day=='Wed'){
            $day='rabu';
        }else if($day=='Thu'){
            $day='kamis';
        }else if($day=='Fri'){
            $day='jumat';
        }else if($day=='Sat'){
            $day='sabtu';
        }else if($day=='Sun'){
            $day='minggu';
        }
    return $day;
}


function conHari($tanggal){
$day=date("D", strtotime ($tanggal)); 
    if($day=='Mon'){
        $day='senin';
    }else if($day=='Tue'){
        $day='selasa';
    }else if($day=='Wed'){
        $day='rabu';
    }else if($day=='Thu'){
        $day='kamis';
    }else if($day=='Fri'){
        $day='jumat';
    }else if($day=='Sat'){
        $day='sabtu';
    }else if($day=='Sun'){
        $day='minggu';
    }
return $day;
}


function getbln($bln){
    switch ($bln) 
    {
        
        case 1:
            return "Januari";
        break;

        case 2:
            return "Februari";
        break;

        case 3:
            return "Maret";
        break;

        case 4:
            return "April";
        break;

        case 5:
            return "Mei";
        break;

        case 6:
            return "Juni";
        break;

        case 7:
            return "Juli";
        break;

        case 8:
            return "Agustus";
        break;

        case 9:
            return "September";
        break;

         case 10:
            return "Oktober";
        break;

        case 11:
            return "November";
        break;

        case 12:
            return "Desember";
        break;
    }

}


function blnAngka($hari){
                if($hari == "Juli"){
                    $hari = "7";
                        return $hari;
                }elseif($hari == "Agustus"){
                    $hari = "8";
                        return $hari;
                }elseif($hari == "September"){
                    $hari = "9";
                        return $hari;
                }elseif($hari == "Oktober"){
                    $hari = "10";
                        return $hari;
                }elseif($hari == "November"){
                    $hari = "11";
                        return $hari;
                }elseif($hari == "Desember"){
                    $hari = "12";
                        return $hari;
                }elseif($hari == "Januari"){
                    $hari = "1";
                        return $hari;
                }elseif($hari == "Februari"){
                    $hari = "2";
                        return $hari;
                }elseif($hari == "Maret"){
                    $hari = "3";
                        return $hari;
                }elseif($hari == "April"){
                    $hari = "4";
                        return $hari;
                }elseif($hari == "Mei"){
                    $hari = "5";
                        return $hari;
                }elseif($hari == "Juni"){
                    $hari = "6";
                        return $hari;
                }
            } 


