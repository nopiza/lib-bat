	<footer class="main-footer text-center">
	    <strong>Copyright &copy; <strong>Admin Web</strong>
	</footer>


</div>
<script src="<?=base_url('theme/plugins/jquery/jquery.min.js');?>"></script> 
<script src="<?=base_url('theme/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<script src="<?=base_url('theme/dist/js/adminlte.min.js');?>"></script>
<script src="<?=base_url('theme/dist/js/Lobibox.js');?>"></script>
<script src="<?=base_url('theme/dist/js/jquery-confirm.min.js'); ?>"></script>
<script src="<?=base_url('theme/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('theme/plugins/datepicker/bootstrap-datepicker.js')?>"></script>

<script>
var url_apps = "<?=base_url();?>"
$(document).ready(function() {
    var refreshId = setInterval(function(){
      $.ajax({
        url: url_apps + 'dashboard/cekpkk/',
        type: 'GET',
        dataType: 'json',
      })
      .done(function(data) {
        if(data.ada){
            $(document).Toasts('create', {
              body: 'Sistem telah menerima data PKK baru dengan rician : <br>' + data.nomor_pkk + '<br>'+ data.nama_perusahaan + '<br>'+ data.nama_kapal + '<br>'+ ' <a href="" class="myLinkClass">Detail</a>',
              title: 'PKK Baru',
              subtitle: 'Notifikasi',
              icon: 'fas fa-envelope fa-lg',
            });
          console.log(data.ada);
        }

      $('a.myLinkClass').click(function() {
        window.toastr.clear();
      });

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    }, 5000);
});
</script>

